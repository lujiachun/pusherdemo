//Auto Generate Don't Edit it
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public partial class TDRedeemReward
    {
        
       
        private EInt m_Id = 0;   
        private string m_Name;   
        private string m_Sprite;   
        private string m_NeedCount;   
        private string m_UnFinishLabel;  
        
        //private Dictionary<string, TDUniversally.FieldData> m_DataCacheNoGenerate = new Dictionary<string, TDUniversally.FieldData>();
      
        /// <summary>
        /// ID
        /// </summary>
        public  int  id {get { return m_Id; } }
       
        /// <summary>
        /// Name
        /// </summary>
        public  string  name {get { return m_Name; } }
       
        /// <summary>
        /// SpriteName图标名字
        /// </summary>
        public  string  sprite {get { return m_Sprite; } }
       
        /// <summary>
        /// 完成需要的数量
        /// </summary>
        public  string  needCount {get { return m_NeedCount; } }
       
        /// <summary>
        /// UnFinishLabel
        /// </summary>
        public  string  unFinishLabel {get { return m_UnFinishLabel; } }
       

        public void ReadRow(DataStreamReader dataR, int[] filedIndex)
        {
          //var schemeNames = dataR.GetSchemeName();
          int col = 0;
          while(true)
          {
            col = dataR.MoreFieldOnRow();
            if (col == -1)
            {
              break;
            }
            switch (filedIndex[col])
            { 
            
                case 0:
                    m_Id = dataR.ReadInt();
                    break;
                case 1:
                    m_Name = dataR.ReadString();
                    break;
                case 2:
                    m_Sprite = dataR.ReadString();
                    break;
                case 3:
                    m_NeedCount = dataR.ReadString();
                    break;
                case 4:
                    m_UnFinishLabel = dataR.ReadString();
                    break;
                default:
                    //TableHelper.CacheNewField(dataR, schemeNames[col], m_DataCacheNoGenerate);
                    break;
            }
          }

        }
        
        public static Dictionary<string, int> GetFieldHeadIndex()
        {
          Dictionary<string, int> ret = new Dictionary<string, int>(5);
          
          ret.Add("Id", 0);
          ret.Add("Name", 1);
          ret.Add("Sprite", 2);
          ret.Add("NeedCount", 3);
          ret.Add("UnFinishLabel", 4);
          return ret;
        }
    } 
}//namespace LR
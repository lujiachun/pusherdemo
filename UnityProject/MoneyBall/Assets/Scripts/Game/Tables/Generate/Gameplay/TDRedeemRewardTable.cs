//Auto Generate Don't Edit it
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public static partial class TDRedeemRewardTable
    {
        private static TDTableMetaData m_MetaData = new TDTableMetaData(TDRedeemRewardTable.Parse, "RedeemReward");
        public static TDTableMetaData metaData
        {
            get { return m_MetaData; }
        }
        
        private static Dictionary<int, TDRedeemReward> m_DataCache = new Dictionary<int, TDRedeemReward>();
        private static List<TDRedeemReward> m_DataList = new List<TDRedeemReward >();
        
        public static void Parse(byte[] fileData)
        {
            m_DataCache.Clear();
            m_DataList.Clear();
            DataStreamReader dataR = new DataStreamReader(fileData);
            int rowCount = dataR.GetRowCount();
            int[] fieldIndex = dataR.GetFieldIndex(TDRedeemReward.GetFieldHeadIndex());
    #if (UNITY_STANDALONE_WIN) || UNITY_EDITOR || UNITY_STANDALONE_OSX
            dataR.CheckFieldMatch(TDRedeemReward.GetFieldHeadIndex(), "RedeemRewardTable");
    #endif
            for (int i = 0; i < rowCount; ++i)
            {
                TDRedeemReward memberInstance = new TDRedeemReward();
                memberInstance.ReadRow(dataR, fieldIndex);
                OnAddRow(memberInstance);
                memberInstance.Reset();
                CompleteRowAdd(memberInstance);
            }
            Log.i(string.Format("Parse Success TDRedeemReward"));
        }

        private static void OnAddRow(TDRedeemReward memberInstance)
        {
            int key = memberInstance.id;
            if (m_DataCache.ContainsKey(key))
            {
                Log.e(string.Format("Invaild,  TDRedeemRewardTable Id already exists {0}", key));
            }
            else
            {
                m_DataCache.Add(key, memberInstance);
                m_DataList.Add(memberInstance);
            }
        }    
        
        public static void Reload(byte[] fileData)
        {
            Parse(fileData);
        }

        public static int count
        {
            get 
            {
                return m_DataCache.Count;
            }
        }

        public static List<TDRedeemReward> dataList
        {
            get 
            {
                return m_DataList;
            }    
        }

        public static TDRedeemReward GetData(int key)
        {
            if (m_DataCache.ContainsKey(key))
            {
                return m_DataCache[key];
            }
            else
            {
                Log.w(string.Format("Can't find key {0} in TDRedeemReward", key));
                return null;
            }
        }
    }
}//namespace LR
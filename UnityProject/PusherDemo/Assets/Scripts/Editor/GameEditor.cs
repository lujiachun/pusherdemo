﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using Qarth;
using Qarth.Editor;

namespace GameWish.Game.Editor
{
    public class GameEditor
    {

        [MenuItem("Assets/Qarth/Config/Build CustomConfig")]
        public static void BuildSDKConfig()
        {
            SDKConfigEditor.BuildConfig<CustomColorConfig>("CustomColorConfig");
        }
        
    }
}
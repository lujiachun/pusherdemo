﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{

    public class RulesPanel : AbstractPanel
    {
        [SerializeField]
        private Button m_CloseBtn;

        protected override void OnUIInit()
        {
            m_CloseBtn.onClick.AddListener(CloseSelfPanel);
        }
    }
}
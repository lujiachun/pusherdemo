﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using System;

namespace GameWish.Game
{

    public class RedeemAmazonPanel : AbstractPanel
    {
        [SerializeField]
        private InputField m_AmazonLabel;
        [SerializeField]
        private Text m_AmountLabel;
        [SerializeField]
        private Text m_NeedAmount;
        [SerializeField]
        private Button m_SumitButton;
        [SerializeField]
        private Image m_TitleImage;
        [SerializeField]
        private Image m_TokenImage;
        [SerializeField]
        private Text m_SideLabel;
        [SerializeField]
        private Button m_CloseButton;

        protected override void OnUIInit()
        {
            m_SumitButton.onClick.AddListener(OnClickSubmitButton);
            m_CloseButton.onClick.AddListener(CloseSelfPanel);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            if (args == null || args.Length <= 0) 
            {
                return;
            }

            var data = args[0] as TDRedeemReward;

            if (data.id == 1)
            {
                m_TitleImage.sprite = FindSprite("WithDrawPanel_amazontitle", true);
                m_TokenImage.sprite = FindSprite("WithDrawPanel_amazon", true);
                m_SideLabel.text = "eMail";
            }
            else
            {
                m_TitleImage.sprite = FindSprite("WithDrawPanel_cashTitle", true);
                m_TokenImage.sprite = FindSprite("WithDrawPanel_Group7", true);
                m_SideLabel.text = "Paypal";

            }
            m_NeedAmount.text = data.needCount;
            m_AmountLabel.text = "";

        }

        private void OnClickSubmitButton()
        {
            string email = m_AmazonLabel.text;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using System;
using Spine.Unity;

namespace GameWish.Game
{

    public class SlotAgainPanel : AbstractPanel
    {
        [SerializeField]
        private Button m_AgainButton;
        [SerializeField]
        private Button m_NoButton;
        [SerializeField]
        private SkeletonGraphic m_Grapghic;

        

        protected override void OnUIInit()
        {
            m_AgainButton.onClick.AddListener(OnAgainButtonClick);
            m_NoButton.onClick.AddListener(CloseSelfPanel);

           
        }

        

        private void ProcessAnim() 
        {
            if (m_Grapghic == null) m_Grapghic = transform.Find("Image/Graphic").GetComponent<SkeletonGraphic>();
            var DD1 = m_Grapghic.AnimationState.SetAnimation(0, "start", false);

            DD1.Complete += (C) =>
            {
                var DD2 = m_Grapghic.AnimationState.SetAnimation(0, "continue", false);
                DD2.Complete += (c) =>
                {
                    var DD3 = m_Grapghic.AnimationState.SetAnimation(0, "end", false);
                    DD3.Complete += (d) =>
                    {
                        ProcessAnim();
                    };
                };
            };
        }

        protected override void OnPanelOpen(params object[] args)
        {
            ProcessAnim();

            m_NoButton.gameObject.SetActive(false);

            Timer.S.Post2Scale(v=> 
            {
                m_NoButton.gameObject.SetActive(true);
            },1,1);
        }

        private void OnAgainButtonClick()
        {
            CustomExtensions.PlayAd("SlotAgain", result => 
            {
                if (result) 
                {
                    //Slot
                    EventSystem.S.Send(EventID.OnRollSlotMachine);
                    CloseSelfPanel();
                }
            });
        }

        
    }
}
﻿using GameWish.Game;
using Qarth;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public enum ROWTYPE 
{
    AAA,
    AAB,
    ABC,
}



public class SlotMachine : WorldUIBindTransform
{

    [SerializeField]
    private SlotMachineItem[] m_MachineItemArray;

    private List<int> m_SlotRewardType = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8 };

    private List<TDLevelRatio> m_LevelRatiosArray = new List<TDLevelRatio>();

    private bool m_SlotState = false;

    private int m_AudioPlayID = -1;
    public bool slotState 
    {
        get 
        {
            return m_SlotState;
        }
    }


    public void Init() 
    {
        for (int i = 0; i < m_SlotRewardType.Count; i++)
        {
            m_LevelRatiosArray.Add(TDLevelRatioTable.GetData(m_SlotRewardType[i]));
        }

        //
        int stage = 1; // TODO 后续需要根据当前等级获取具体的stage

        for (int i = 0; i < m_MachineItemArray.Length; i++)
        {
            m_MachineItemArray[i].Init();
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="stage">求取方式为 GameData.CurrentDolloar</param>
    /// <returns></returns>
    public int GetResultIndex(int stage) 
    {
        List<int> list = new List<int>(); //(List<int>) from item in m_LevelRatiosArray select item.GetRatioNumByStage(stage);

        for (int i = 0; i < m_LevelRatiosArray.Count; i++)
        {
            list.Add(m_LevelRatiosArray[i].GetRatioNumByStage(stage));
        }

        int index = GameExtensions.RandomByWeightIndex(list) + 1;

        return TDSlotRewardTable.GetItemByRatioIndex(index).id;
    }

    /// <summary>
    /// 不能中奖时使用
    /// </summary>
    /// <param name="lastIndex"></param>
    /// <returns></returns>
    public int GetRandomIndex(int lastIndex) 
    {
        int id = RandomHelper.Range(1, 10);
        return id == lastIndex ? GetRandomIndex(lastIndex) : id;
    }

    public IEnumerator Roll() 
    {
        //if (m_SlotState) yield break;

        m_SlotState = true;
        m_AudioPlayID = AudioMgr.S.PlaySound("slot-spin", true);
        for (int i = 0; i < m_MachineItemArray.Length; i++)
        {
            m_MachineItemArray[i].Roll();
            yield return new WaitForSeconds(0.5f);
        }

        
         
        
        Timer.S.Post2Scale(v=> 
        {
            
            
            StartCoroutine(Stop());
        },1,1);
    }

    public IEnumerator Stop() 
    {
        AudioMgr.S.Stop(m_AudioPlayID);
        int id =  AudioMgr.S.PlaySound("slot-stop", true);
        //TODO  1、根据规则先确定本次转盘转动是否能获得奖励
        bool canWin = RandomHelper.Range(1,100) <= 50;
        //2、不能的话则
        m_SlotState = false;
        if (canWin)
        {

            int stage = ((int)GameInfoMgr.data.GetDollar() / 10) + 1;
            int index = GetResultIndex(stage);
            for (int i = 0; i < m_MachineItemArray.Length; i++)
            {
                m_MachineItemArray[i].Stop(index - 1);
                yield return new WaitForSeconds(0.5f);
            }
            yield return new WaitForSeconds(2.5f);


            ProcessWin(index);

        }
        else
        {
            int lastindex = -1;
            for (int i = 0; i < m_MachineItemArray.Length; i++)
            {
                lastindex = GetRandomIndex(i == 2 ? lastindex : -1);

                m_MachineItemArray[i].Stop(lastindex - 1);
                yield return new WaitForSeconds(0.5f);
            }
            yield return new WaitForSeconds(2.5f);
            // UIMgr.S.OpenPanel(UIID.SlotFailedPanel);

            m_SlotState = false;


           
        }

        AudioMgr.S.Stop(id);
    }

    private void ProcessWin(int id)
    {
        var item = TDSlotRewardTable.GetData(id);
        var type = CustomExtensions.GetStringEnum<SlotRewardType>(item.reardType);

        Debug.LogError(item.reardType);
        switch (type)
        {
            case SlotRewardType.AmazonCard:
                GameInfoMgr.data.AddAmazomCount(1);
                break;
            case SlotRewardType.Joyticks:
                EventSystem.S.Send(EventID.AllocateDecoration);
                break;
            case SlotRewardType.Prize777:
                EventSystem.S.Send(EventID.OnSlotReward, type);
                break;
            default:
                EventSystem.S.Send(EventID.OnSlotReward, type);
                break;
        }
            

    }
}

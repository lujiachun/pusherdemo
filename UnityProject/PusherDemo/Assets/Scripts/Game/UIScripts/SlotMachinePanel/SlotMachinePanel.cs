﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System;
using UnityEngine.UI;
using DG.Tweening;

namespace GameWish.Game
{
    public class SlotMachinePanel : AbstractPanel
    {
        [SerializeField]
        private SlotMachine m_SlotMachineRoot;
        [SerializeField]
        private Transform m_SlotCount;
        [SerializeField]
        private Text m_LeftTimesLabel;
        [SerializeField]
        private Transform m_DoubleTrans;
        [SerializeField]
        private Transform m_NormalTrans;
        [SerializeField]
        private Text m_ProcessLabel;
        [SerializeField]
        private Image m_TaskImage;
        [SerializeField]
        private Slider m_ProcessSlider;
        [SerializeField]
        private Image m_CountDownProcess;
        [SerializeField]
        private Slider m_CountDownSlider;

        private int m_CountDown;

        private TDTask m_CurrentTask;
        private int m_CurrentCount = 0;

        private Coroutine m_Progress;

        protected override void OnOpen()
        {
            RegisterEvent(EventID.OnRollSlotMachine, OnRollSlotMachine);
            m_SlotMachineRoot.followTransform = GameObject.Find("SlotMachineAnchor").transform;
            RegisterEvent(EventID.OnTaskChange, OnTaskChange);
            RegisterEvent(EventID.OnSlotReward, OnSlotReward);

            TaskMgr.S.InitTask();
        }

        private void OnSlotReward(int key, params object[] param)
        {
            if (param == null || param.Length == 0)
            {
                return;
            }

            var data = (SlotRewardType)param[0];

            //if (m_CurrentTask == null || string.IsNullOrEmpty(m_CurrentTask.reardType)) return;

            Debug.LogError("data : " + data + "  m_CurrentTask.reardType : " + m_CurrentTask.reardType);

            if (data.ToString() == m_CurrentTask.reardType) 
            {
                m_CurrentCount++;

                m_ProcessLabel.text = m_CurrentCount + "/" + m_CurrentTask.needCount;
                m_ProcessSlider.value = (float)m_CurrentCount/ m_CurrentTask.needCount;

                if (m_CurrentCount >= m_CurrentTask.needCount) 
                {

                    EventSystem.S.Send(EventID.OnDoubleModeStateChange, true);

                    if (m_Progress != null) 
                    {
                        StopCoroutine(m_Progress);
                    }

                    m_Progress = StartCoroutine(DoCountDownProgress(1200));

                }
            }
        }

        private void OnTaskChange(int key, params object[] param)
        {
            if (param == null || param.Length == 0) 
            {
                return;
            }

            var data = param[0] as TDTask;
            m_CurrentCount = 0;
            m_CurrentTask = data;
            m_ProcessLabel.text = m_CurrentCount + "/" + m_CurrentTask.needCount;
            m_ProcessSlider.value = 0;
            m_TaskImage.sprite = FindSprite(data.sprite, true);
            m_TaskImage.SetNativeSize();

        }

        protected override void OnUIInit()
        {
            m_SlotMachineRoot.Init();
            m_DoubleTrans.gameObject.SetActive(false);
            m_NormalTrans?.gameObject.SetActive(true);
        }

        private void OnRollSlotMachine(int key,params object[] param)
        {
            m_SlotCount.DOLocalMoveX(324, 1f).OnComplete(()=> 
            {
                Timer.S.Post2Scale((v)=> 
                {
                    m_SlotCount.DOLocalMoveX(424, 1f);
                },1);
            });
            
            //if (m_SlotMachineRoot.slotState) return;

            m_SlotMachineRoot.StartCoroutine(m_SlotMachineRoot.Roll());
        }

        //refactor 控制逻辑集中处理  UI只处理UI表现
        IEnumerator DoCountDownProgress(int time) 
        {
            AudioMgr.S.PlayBg("BGM-2x");
            int times = 0;
            m_DoubleTrans.gameObject.SetActive(true);
            m_NormalTrans?.gameObject.SetActive(false);
            m_CountDownProcess.fillAmount = 1;
            while (time >= times)
            {
                yield return new WaitForSeconds(0.1f);
                times++;
                m_CountDownProcess.fillAmount = 1 - (float)times / time;
                m_CountDownSlider.value = 1 - (float)times / time;
            }

            //结束 准备切回正常得
            m_DoubleTrans.gameObject.SetActive(false);
            m_NormalTrans?.gameObject.SetActive(true);

            EventSystem.S.Send(EventID.OnDoubleModeStateChange, false);
            TaskMgr.S.PostNextTask();

            //

            AudioMgr.S.PlayBg("BGM-normal");
        }
    }
}
   
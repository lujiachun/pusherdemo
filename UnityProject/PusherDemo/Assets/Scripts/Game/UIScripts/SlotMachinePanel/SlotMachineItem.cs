﻿
using GameWish.Game;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;
using UnityEngine.UI;



public class SlotMachineItem : MonoBehaviour
{

    public List<Image> sprites = new List<Image>();
    private Image lastSprite;
    private int indexResetPos;
    private int indexMove;
    public float m_Speed = 0;

    public float m_MoveDis;
    public int m_LastResultID;
    public int m_ResultID;
    private float m_ImageSizeY = 0;
    private bool m_StartRoll = false;
    private bool m_Init =false;
    private ResLoader m_Loader;

    private void Awake()
    {
        //GetAll();
    }

    private void Update()
    {
        if (!m_Init) { return; }

        Move();
        ResetPos();
    }


    /// <summary>
    /// 滚动
    /// </summary>
    private void Move()
    {
        if (m_MoveDis < 0)
        {
            m_Speed = 0;
            StartCoroutine(ReturnZero());
            m_MoveDis = m_MoveDis += m_ImageSizeY * sprites.Count;
        }

        if (m_StartRoll && m_MoveDis < 0.2f)
        {
            m_MoveDis += m_ImageSizeY * sprites.Count;
        }

        if (m_Speed > 0)
        {
            m_MoveDis -= 50f;
        }

        for (indexMove = 0; indexMove < sprites.Count; indexMove++)
        {
            sprites[indexMove].transform.localPosition = new Vector3(sprites[indexMove].transform.localPosition.x, sprites[indexMove].transform.localPosition.y, sprites[indexMove].transform.localPosition.z) + new Vector3(0, 1, 0) * m_Speed;
        }
    }

    private int currentCheck;

    /// <summary>
    /// 重置坐标
    /// </summary>
    private void ResetPos()
    {

        if (sprites[currentCheck].transform.localPosition.y > 180) 
        {
            int last = (currentCheck + 9 - 1) % 9;

            sprites[currentCheck].transform.localPosition = new Vector3(sprites[last].transform.localPosition.x, sprites[last].transform.localPosition.y - m_ImageSizeY, sprites[last].transform.localPosition.z);
            currentCheck++;
            if (currentCheck >= 9) 
            {
                currentCheck = 0;
            }
        }

/*        if ( sprites[indexResetPos].transform.localPosition.y > m_ImageSizeY)
        {
            sprites[indexResetPos].transform.localPosition = new Vector3(lastSprite.transform.localPosition.x, lastSprite.transform.localPosition.y - m_ImageSizeY, lastSprite.transform.localPosition.z);
            lastSprite = sprites[indexResetPos];
            indexResetPos++;

            if (indexResetPos >= sprites.Count)
                indexResetPos = 0;
        }*/
    }

    /// <summary>
    /// 停止滚动
    /// </summary>
    private void StopMove()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (m_Speed < 10)
                return;

            StartCoroutine(SlowDown(0.5f));
        }
    }

    public void Stop(int stopID) 
    {

        Debug.LogError("stopID == "  + stopID + " indexResetPos==" + indexResetPos);
        m_ResultID = stopID;
        m_MoveDis += m_ImageSizeY *((stopID + sprites.Count) - indexResetPos);
        m_StartRoll = false;
    }
    public void Roll()
    {
        m_MoveDis += m_ImageSizeY * sprites.Count;
        m_StartRoll = true;
        m_Speed = 50;
    }
    public void Init()
    {
        if (m_Init) return;
        m_Loader = ResLoader.Allocate("RollSlot");
        m_ImageSizeY = 150;
        for (int i = 0; i < TDSlotRewardTable.count; i++)
        {
            GameObject obj = new GameObject();
            Image sprite = obj.AddMissingComponent<Image>();
            sprite.sprite = m_Loader.LoadSync(TDSlotRewardTable.dataList[i].sprite) as Sprite;
            sprite.SetNativeSize();
            obj.transform.SetParent(transform);      
            obj.transform.localPosition = new Vector3(0, -m_ImageSizeY * i, 0);
            obj.transform.localEulerAngles = Vector3.zero;
            obj.transform.localScale = Vector3.one;
            sprites.Add(sprite);
        }
        lastSprite = sprites[sprites.Count - 1];
        currentCheck = 0;
        m_Init = true;
    }

    /// <summary>
    /// 减速
    /// </summary>
    /// <param name="del"></param>
    /// <returns></returns>
    IEnumerator SlowDown(float del)
    {
        while (m_Speed > 0)
        {
            yield return new WaitForSeconds(0.1f);
            m_Speed -= del;
            if (m_Speed <= 0)
            {
                StartCoroutine(ReturnZero());
            }
        }
    }

    /// <summary>
    /// 位置归零
    /// </summary>
    /// <returns></returns>
    IEnumerator ReturnZero()
    {
        float dis = 135;
        int id = 0;

        /*        for (int j = 1; j < sprites.Count; j++)
                {
                    if (Mathf.Abs(sprites[j].transform.localPosition.y) < Mathf.Abs(dis))
                    {
                        dis = sprites[j].transform.localPosition.y;
                        id = j;
                        m_LastResultID = j;
                    }
                }*/

        id = m_ResultID;
        dis = sprites[id].transform.localPosition.y;
        float  multi = Mathf.Abs(dis / 150);

        while (Mathf.Abs(sprites[id].transform.localPosition.y) > 9f)
        {
            yield return null;

            for (int k = 0; k < sprites.Count; k++)
            {
                sprites[k].transform.localPosition -= new Vector3(0, dis * Time.deltaTime, 0);
            }

            //ResetPos();
        }

        //Reset();
    }

    private void Reset()
    {
        m_Init = false;

        for (int i = 0; i < sprites.Count; i++)
        {
            Destroy(sprites[i].gameObject);
        }

        sprites = new List<Image>();
        Init();
/*        m_LastResultID = 0;
        m_ResultID = 0;
        m_MoveDis = 0;*/
    }
}
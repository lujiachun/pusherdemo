﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{

    public enum SlotRewardType
    {
        D50,        //50绿币
        D20,        //20绿币
        G100,       //100金币
        G50,        //50金币
        G25,        //25金币
        D777,       //777美金
        Prize777,   //777大奖
        Joyticks,   // 摇杆
        AmazonCard, //亚马逊卡
    }
}
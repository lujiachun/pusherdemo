﻿using DG.Tweening;
using GameWish.Game;
using Qarth;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FloatPoolPanel : AbstractPanel
{
    [SerializeField]
    private Transform[] m_PathArray;
    [SerializeField]
    private Button m_PoolButton;
    Vector3[] vector3s;

    protected override void OnUIInit()
    {
        m_PoolButton.onClick.AddListener(OnClickPoolButton);

        vector3s = new Vector3[m_PathArray.Length];
        for (int i = 0; i < m_PathArray.Length; i++)
        {
            vector3s[i] = m_PathArray[i].position;
        }
    }

    protected override void OnPanelOpen(params object[] args)
    {
        Move();
    }

    private void OnClickPoolButton()
    {
        CustomExtensions.PlayAd("FloatPool", ok => 
        {
            if (ok) 
            {
                //TODO 发送时间后续每次点击50概率出现绿币
                EventSystem.S.Send(EventID.OnProcessHalfGreen, true);
                // 关闭自身界面
                CloseSelfPanel();
            }
        });
    }

    void Move()    
    {
        m_PoolButton.transform.position = m_PathArray[0].position;
        m_PoolButton.transform.DOPath(vector3s, 35f, PathType.Linear).SetLoops(-1).SetEase(Ease.Linear);
    }
}

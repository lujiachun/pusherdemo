﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
     
    public class WorldUIPanel : AbstractPanel
    {
        public static WorldUIPanel S;

        [SerializeField]
        private WorldUI_QuitTrigger m_TriggerPrefab;

        [SerializeField]
        private Image m_ImgFocus;

        protected override void OnUIInit()
        {

            GameObjectPoolMgr.S.AddPool("QuitTrigger", m_TriggerPrefab.gameObject , 10, 9);

            base.OnUIInit();
            S = this;
            QuitTriggerMgr.S.InitMgr();

        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
        }

        protected override void OnClose()
        {
            base.OnClose();
        }

        public WorldUI_QuitTrigger AllocateQuitTrigger(Transform followTrans) 
        {
            var go = GameObjectPoolMgr.S.Allocate("QuitTrigger").GetComponent<WorldUI_QuitTrigger>();

            if (go != null) 
            {
                go.followTransform = followTrans;
                go.targetUI = go.transform;
                go.transform.SetParent(transform);
                go.Init(this);
            }


            return go;
        }



    }
}

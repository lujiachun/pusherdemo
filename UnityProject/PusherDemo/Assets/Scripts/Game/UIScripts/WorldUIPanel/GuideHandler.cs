using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Qarth;

namespace GameWish.Game
{
    public class GuideHandler : WorldUIBindPos
    {
        [SerializeField]
        private ImageFrameAnim m_Hand;

        [SerializeField]
        private Image m_HandImg;
        [SerializeField]
        private Image m_Point;
        [SerializeField]
        private Image m_Line;

        private Vector3 m_InitPos;

        void Awake()
        {
            m_Hand.gameObject.SetActive(false);
            m_Point.gameObject.SetActive(false);
            m_Line.gameObject.SetActive(false);
        }

        public void Show()
        {
            m_Hand.gameObject.SetActive(true);
            m_InitPos = m_Hand.transform.localPosition;
            m_Hand.Play();

            m_Point.gameObject.SetActive(true);
            m_Line.gameObject.SetActive(true);
            m_Hand.transform.DOLocalMoveY(-200, 1f).SetLoops(-1, LoopType.Restart);
        }

        public void Hide()
        {
            m_Hand.transform.DOKill();
            m_Hand.transform.localPosition = m_InitPos;
            m_Hand.gameObject.SetActive(false);
            m_Point.gameObject.SetActive(false);
            m_Line.gameObject.SetActive(false);
        }

        void OnHandClick(bool rev)
        {

        }
    }
}

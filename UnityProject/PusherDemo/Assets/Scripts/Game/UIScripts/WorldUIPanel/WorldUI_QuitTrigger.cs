﻿using GameWish.Game;
using Qarth;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldUI_QuitTrigger : WorldUIBindTransform
{
    [SerializeField]
    private Text m_Text;
    [SerializeField]
    private Image m_Decoration;

    private AbstractPanel m_Panel;
    private int DecorateID;

    public void Init(AbstractPanel panel) 
    {
        m_Panel = panel;
    }

    public void UpdateValue(int multi) 
    {
        m_Text.text = "x" + multi;
        if (!m_Text.gameObject.activeSelf) 
        {
            m_Text.gameObject.SetActive(true);
        }

    }

    public void SetDecoration(int id) 
    {
        var dat = TDDecorationsTable.GetData(id);
        DecorateID = id;
        if (dat == null) 
        {
            m_Decoration.gameObject.SetActive(false);
        }
        m_Text.gameObject.SetActive(false);
        m_Decoration.gameObject.SetActive(true);
        m_Decoration.sprite = m_Panel.FindSprite(dat.sprite, true);
    }

    public void HideDecoration() 
    {
        if (!m_Decoration.gameObject.activeSelf)
        {
            return;
        }

        AudioMgr.S.PlaySoundShoot("propget");
        

        //TODO 收集特效
        GameInfoMgr.data.AddDecorations(DecorateID);
        m_Decoration.gameObject.SetActive(false);
        m_Text.gameObject.SetActive(true);
    }

}

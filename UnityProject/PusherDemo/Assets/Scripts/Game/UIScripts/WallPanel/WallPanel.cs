﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{

    public class WallPanel : AbstractPanel
    {
        [SerializeField]
        private Button m_CloseButton;
        [SerializeField]
        private Button m_FreeButton;
        [SerializeField]
        private Text m_RemainingLabel;


        protected override void OnUIInit()
        {
            m_CloseButton.onClick.AddListener(CloseSelfPanel);
            m_FreeButton.onClick.AddListener(OnFreeAdButtonClick);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            
        }

        /// <summary>
        /// 播放广告后打开墙壁
        /// </summary>
        private void OnFreeAdButtonClick()
        {
            CustomExtensions.PlayAd("NoTokens", b =>
            {
                if (b)
                {
                    EventSystem.S.Send(EventID.OnWallOpen);
                    CloseSelfPanel();
                    // 180秒后关闭墙
                    Timer.S.Post2Scale(v => 
                    { 
                        EventSystem.S.Send(EventID.OnWallClose);
                    }, 5f);
                }
            });
        }


        
    }
}
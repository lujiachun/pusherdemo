﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using System;

namespace GameWish.Game
{

    public class RedeemPanel : AbstractPanel
    {
        [SerializeField]
        private Button m_BackBtn;
        [SerializeField]
        private Button m_MenuBtn;
        [SerializeField]
        private Button m_RuleBtn;
        [SerializeField]
        private Button m_LinkToFB;

        [SerializeField]
        private Text m_UsrNameLabel;
        [SerializeField]
        private Text m_IDLabel;
        [SerializeField]
        private USimpleListView m_ListView;

        protected override void OnUIInit()
        {
            m_BackBtn.onClick.AddListener(CloseSelfPanel);
            m_MenuBtn.onClick.AddListener(OnClickMenuBtn);
            m_RuleBtn.onClick.AddListener(OnClickRuleBen);
            m_LinkToFB.onClick.AddListener(OnClickLinktoFBBtn);
            m_ListView.SetCellRenderer(OnCellRenderer);
            m_ListView.SetDataCount(TDRedeemRewardTable.dataList.Count);
        }

        private void OnCellRenderer(Transform root,  int index)
        {
            var ad = root.GetComponent<RedeemItemPrefab>();
            ad.InitPrefab(TDRedeemRewardTable.dataList[index], this);
        }

        private void OnClickMenuBtn()
        {

        }

        private void OnClickRuleBen()
        {

        }

        private void OnClickLinktoFBBtn()
        {

        }
    }
}
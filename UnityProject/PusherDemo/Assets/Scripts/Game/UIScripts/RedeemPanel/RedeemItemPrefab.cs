﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using System;

namespace GameWish.Game
{

    public class RedeemItemPrefab : UListItemView
    {
        [SerializeField]
        private Image m_ItemIcon;
        [SerializeField]
        private Text m_TitleLabel;
        [SerializeField]
        private Text m_ProcessLabel;
        [SerializeField]
        private Image m_ProcessFillImg;
        [SerializeField]
        private Button m_Button;
        [SerializeField]
        private Image m_StateImage;


        public void InitPrefab(TDRedeemReward data , AbstractPanel panel) 
        {
            m_ItemIcon.sprite = panel.FindSprite(string.IsNullOrEmpty(data.sprite) ? "": data.sprite, true);
            var data1 = GameInfoMgr.data.GetRedeemRewardData(data.id);

            m_StateImage.gameObject.SetActive(data1.isFinish);

            m_ProcessLabel.text = string.Format("{0}/{1}", data1.current, data.needCount);

            m_ProcessFillImg.fillAmount = (float)data1.current / (float)data.GetGoalValue();
            m_TitleLabel.text = data.name;

            m_Button.onClick.AddListener(()=> 
            {
                if (data1.current >= (float)data.GetGoalValue() && !data1.isFinish)
                {
                    UIMgr.S.OpenPanel(UIID.RedeemWithdrawPanel, data);
                }
                else if(data1.current <= (float)data.GetGoalValue())
                {
                    UIMgr.S.OpenPanel(UIID.SorryPanel, data); 
                }
            });
        }


    }
}
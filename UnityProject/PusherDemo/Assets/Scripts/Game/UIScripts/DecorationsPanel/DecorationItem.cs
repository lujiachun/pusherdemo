﻿using GameWish.Game;
using Qarth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecorationItem : MonoBehaviour
{
    [SerializeField]
    private Image m_CoreImg;
    [SerializeField]
    private Material m_Material;

    public void InitDecoration(TDDecorations decoration, AbstractPanel panel) 
    {
        m_CoreImg.sprite = panel.FindSprite(decoration.sprite, true);
        if (!GameInfoMgr.data.isHasDecorations(decoration.id)) 
        {
            m_CoreImg.material = m_Material;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{

    public class DecorationItemGroup : IUListItemView
    {
        [SerializeField]
        private Transform m_Root;
        [SerializeField]
        private DecorationItem m_Item;

        public void InitGroup(int index, AbstractPanel panel) 
        {
            if (m_Root.childCount != 0) 
            {
                m_Root.GetChildTrsList().ForEach(v => 
                {
                    Destroy(v.gameObject);
                });
            }

            for (int i = 0; i < 4; i++)
            {
                if (index + i >= TDDecorationsTable.dataList.Count) break;

                var data = TDDecorationsTable.dataList[index + i];
                var go = Instantiate(m_Item.gameObject, m_Root).GetComponent<DecorationItem>();

                go.InitDecoration(data, panel);
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using System;

namespace GameWish.Game
{

    public class DecorationsPanel : AbstractPanel
    {
        [SerializeField]
        private USimpleListView m_ListView;
        [SerializeField]
        private Button m_BackBtn;
        [SerializeField]
        private Button m_MenuBtn;
        [SerializeField]
        private Button m_QABtn;


        protected override void OnPanelOpen(params object[] args)
        {
            int count = TDDecorationsTable.count % 4;
            count = count == 0 ? TDDecorationsTable.count / 4 : TDDecorationsTable.count / 4 + 1;
            m_ListView.SetDataCount(count);
        }

        protected override void OnUIInit()
        {
            m_BackBtn.onClick.AddListener(CloseSelfPanel);
            m_MenuBtn.onClick.AddListener(OnClickMenuBtn);
            m_QABtn.onClick.AddListener(OnClickQABtn);


            m_ListView.SetCellRenderer(OnCellRenderer);
        }

        private void OnCellRenderer(Transform root, int index)
        {
            var rc = root.GetComponent<DecorationItemGroup>();
            rc.InitGroup(index * 4 , this);
        }

        private void OnClickMenuBtn()
        {
            
        }

        private void OnClickQABtn()
        {
            
        }
    }
}
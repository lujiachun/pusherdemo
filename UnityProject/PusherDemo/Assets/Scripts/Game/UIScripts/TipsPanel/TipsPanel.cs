﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using System;

namespace GameWish.Game
{

    public enum TIPSTYPE 
    {
        Lighting,
        Clear,
        NoTokens
    }

    public class TipsPanel : AbstractPanel
    {
        [SerializeField]
        private Text m_TitleLabel;
        [SerializeField]
        private Text m_DescriptionLabel;
        [SerializeField]
        private Button m_CloseButton;
        [SerializeField]
        private Button m_FreeButton;
        [SerializeField]
        private Image m_BGImg;
        [SerializeField]
        private Image m_ItemIcon;

        private TIPSTYPE m_TipsType = TIPSTYPE.Clear;

        private string[] m_ColorArray = new string[] { "ff7a3d", "8249B4", "c29200"};
        private string[] m_BGSpriteArray = new string[] { "TipsPanel_OrangeBg", "TipsPanel_GreayBg", "TipsPanel_YellowBg" };
        private string[] m_ItemSpriteArray = new string[] { "TipsPanel_Group3", "TipsPanel_Group2copy", "TipsPanel_Group2" };

        protected override void OnUIInit()
        {
            m_CloseButton.onClick.AddListener(CloseSelfPanel);
            m_FreeButton.onClick.AddListener(OnFreeAdButtonClick);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            if (args.Length <= 0 || args == null) 
            {
                CloseSelfPanel();
                return;
            }

            m_TipsType = (TIPSTYPE)args[0];

            InitUIWithTipsType();
        }

        private void InitUIWithTipsType() 
        {
            GetDescription(m_TipsType);
            GetTitle(m_TipsType);
            GetBg(m_TipsType);
        }

        private void GetBg(TIPSTYPE m_TipsType)
        {

            int index = (int)m_TipsType;
            Color color = color = CustomExtensions.HexToColor(m_ColorArray[index]);
            Sprite sprite = FindSprite(m_BGSpriteArray[index], true);

            m_DescriptionLabel.color = color;
            m_BGImg.sprite = sprite;
            m_ItemIcon.sprite = FindSprite(m_ItemSpriteArray[index], true);
            m_ItemIcon.SetNativeSize();
        }

        private void GetTitle(TIPSTYPE type) 
        {
            string title = "";
            switch (type)
            {
                case TIPSTYPE.Lighting:
                    title = "Lighting";
                    break;
                case TIPSTYPE.Clear:
                    title = "Clear";
                    break;
                case TIPSTYPE.NoTokens:
                    title = "Tokens Not Enough";
                    break;
                default:
                    break;
            }

            m_TitleLabel.text = title;
        }

        private void GetDescription(TIPSTYPE type)
        {
            string des = "";
            switch (type)
            {
                case TIPSTYPE.Lighting:
                    des = "Lighting";
                    break;
                case TIPSTYPE.Clear:
                    des = "Clear";
                    break;
                case TIPSTYPE.NoTokens:
                    des = "Tokens Not Enough";
                    break;
                default:
                    break;
            }

            m_DescriptionLabel.text = des;
        }



        private void OnFreeAdButtonClick()
        {
            CustomExtensions.PlayAd(m_TipsType.ToString(), b => 
            {
                if (b)
                {
                    if (m_TipsType == TIPSTYPE.Clear)
                    {
                        //TODO 发送事件处理清除  false
                        EventSystem.S.Send(EventID.OnObstacleClearStateChange, false);
                    }
                    else if (m_TipsType == TIPSTYPE.Lighting)
                    {
                        //TODO  发送时间处理炫光
                        EventSystem.S.Send(EventID.OnLightingActive);
                    }
                    else if (m_TipsType == TIPSTYPE.NoTokens) 
                    {
                        GameInfoMgr.data.AddToken(40);
                    }
                }

                CloseSelfPanel();
            });
        }
    }
}
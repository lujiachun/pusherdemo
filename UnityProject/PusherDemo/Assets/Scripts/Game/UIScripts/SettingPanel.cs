﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using System;

namespace GameWish.Game
{
    public class SettingPanel : AbstractPanel
    {
        [SerializeField]
        private Button m_CloseBtn;
        [SerializeField]
        private Button m_SoundBtn;
        [SerializeField]
        private Button m_InfoBtn;
        [SerializeField]
        private Button PrivacyPolicyBtn;


        protected override void OnUIInit()
        {
            m_CloseBtn.onClick.AddListener(CloseSelfPanel);
            m_SoundBtn.onClick.AddListener(OnSoundBtnClick);
            m_InfoBtn.onClick.AddListener(OnInfoBtnClick);
            PrivacyPolicyBtn.onClick.AddListener(OnPPBtnClick);
        }

        private void OnSoundBtnClick()
        {

        }

        private void OnInfoBtnClick()
        {

        }

        private void OnPPBtnClick()
        {
            
        }
    }
}
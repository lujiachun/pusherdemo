﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using System;

namespace GameWish.Game {

    public class RedeemRecordPanel : AbstractPanel
    {
        [SerializeField]
        private Transform m_RectContent;

        [SerializeField]
        private Button m_BackBtn;
        [SerializeField]
        private USimpleListView m_ListView;
        protected override void OnUIInit()
        {
            m_BackBtn.onClick.AddListener(CloseSelfPanel);
            m_ListView.SetCellRenderer(OnCellRenderer);
        }

        private void OnCellRenderer(Transform root, int index)
        {
            var ietm = root.GetComponent<RedeemRecordItem>();

            ietm.Init(GameInfoMgr.data.m_RedeemDataArray[index], index == GameInfoMgr.data.m_RedeemDataArray.Count - 1);
        }

        protected override void OnOpen()
        {
            RegisterEvent(EventID.OnRedeemRecordUpdate, OnRedeemRecordUpdate);
        }

        private void OnRedeemRecordUpdate(int key, params object[] param)
        {
            m_ListView.SetDataCount(GameInfoMgr.data.m_RedeemDataArray.Count);
        }
    }
}
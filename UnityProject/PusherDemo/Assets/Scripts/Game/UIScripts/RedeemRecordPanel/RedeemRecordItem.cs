﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;

namespace GameWish.Game
{

    public class RedeemRecordItem : MonoBehaviour
    {
        [SerializeField]
        private Text m_ItemName;
        [SerializeField]
        private Text m_timeLabel;
        [SerializeField]
        private Text StateLabel;
        [SerializeField]
        private Transform m_LineTrans;


        public void Init(RedeemRecordData data , bool lastone = false) 
        {
            m_ItemName.text = data.ItemName;
            m_timeLabel.text = data.Time;
            StateLabel.text = data.State;

            m_LineTrans.gameObject.SetActive(!lastone);
        }
    }
}
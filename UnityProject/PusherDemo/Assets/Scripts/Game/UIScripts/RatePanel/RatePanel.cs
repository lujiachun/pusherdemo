﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public class RatePanel : AbstractAnimPanel
    {
        [SerializeField]
        private Toggle[] m_StarToggles;
        protected override void OnUIInit()
        {
            base.OnUIInit();
            for (int i = 0; i < m_StarToggles.Length; i++)
            {
                int index = i;
                m_StarToggles[i].onValueChanged.AddListener((bool isSelect) =>
                {
                    if (isSelect)
                    {
                        ShowStar(index);
                    }
                });
            }
        }

        protected override void OnOpen()
        {
            base.OnOpen();
            OpenDependPanel(EngineUI.MaskPanel, -1, null);
        }

        protected override void OnPanelHideComplete()
        {
            base.OnPanelHideComplete();
            CloseSelfPanel();
            PlayerPrefs.SetInt("HasRate", 1);
        }

        public override BackKeyCodeResult OnBackKeyDown()
        {
            HideSelfWithAnim();
            return BackKeyCodeResult.PROCESS_AND_BLOCK;
        }

        private void ShowStar(int index)
        {
            DataAnalysisMgr.S.CustomEvent("RateStar", index.ToString());
            for (int i = 0; i <= index; i++)
            {
                m_StarToggles[i].isOn = true;
            }

            if (index >= 4)
            {
                Log.i("OpenMarketRatePage");
                SocialMgr.S.OpenMarketRatePage();
            }
            HideSelfWithAnim();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{

    public class TokensNotEnoughPanel : AbstractPanel
    {
        [SerializeField]
        private Button m_CloseButton;
        [SerializeField]
        private Button m_FreeButton;
        [SerializeField]
        private Text m_RemainingLabel;


        protected override void OnUIInit()
        {
            m_CloseButton.onClick.AddListener(CloseSelfPanel);
            m_FreeButton.onClick.AddListener(OnFreeAdButtonClick);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            
        }

        private void OnFreeAdButtonClick()
        {
            CustomExtensions.PlayAd("NoTokens", b =>
            {
                if (b)
                {
                    GameInfoMgr.data.AddToken(40);
                    CloseSelfPanel();
                }
            });

        }
    }
}
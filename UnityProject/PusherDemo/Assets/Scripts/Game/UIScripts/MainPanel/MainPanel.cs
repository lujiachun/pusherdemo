﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using GameWish.Game;
using System;

public class MainPanel : AbstractPanel
{
    // [SerializeField]
    // private Text m_AmazonLabel;
    [SerializeField]
    private Text m_TokensLabel;
    [SerializeField]
    private Text m_CoinsLabel;
    [SerializeField]
    private Text m_DolloarLabel;
    [SerializeField]
    private Text m_NextInTimeLabel;
    [SerializeField]
    private Button m_ShopBtn;
    [SerializeField]
    private Button m_ShakeBtn;
    [SerializeField]
    private Button m_WallBtn;
    [SerializeField]
    private Button m_SettingButton;
    // [SerializeField]
    // private Button m_CoinsRedeemOpenBtn;
    // [SerializeField]
    // private Button m_DolloarRedeemOpenBtn;
    // [SerializeField]
    // private Button m_AmazonsRedeemOpenBtn;

    [SerializeField]
    private Transform m_ClearCountDownTrans;
    [SerializeField]
    private Text m_ClearCountDownLabel;
    [SerializeField]
    private Text m_LightingCoolingDownLabel;
    [SerializeField]
    private Text m_TokenInTimeLabel;

    private int m_TimesID = -1;

    private static int Times = 60;
    protected override void OnUIInit()
    {
        // m_TokensLabel.text = GameInfoMgr.data.GetTokenCount().ToString();
        m_CoinsLabel.text = GameInfoMgr.data.GetCoinsCount().ToString();
        m_DolloarLabel.text = GameInfoMgr.data.GetDollar().ToString();
        m_ShopBtn.onClick.AddListener(() => {
            UIMgr.S.OpenPanel(UIID.RedeemPanel);
        });
        m_WallBtn.onClick.AddListener(() => {
            UIMgr.S.OpenPanel(UIID.WallPanel);
        });
        m_ShakeBtn.onClick.AddListener(() => {
            UIMgr.S.OpenPanel(UIID.WallPanel);
        });
        

        // m_LightingCoolingDownLabel.gameObject.SetActive(false);
        // m_ClearCountDownTrans.gameObject.SetActive(false);
        // m_NextInTimeLabel.gameObject.SetActive(false);

        // m_DecorationBtn.onClick.AddListener(()=> 
        // {
        //     UIMgr.S.OpenPanel(UIID.RedeemPanel);
        // });

        // m_ClearButton.onClick.AddListener(() =>
        // {
        //     UIMgr.S.OpenPanel(UIID.TipsPanel, TIPSTYPE.Clear);
        // });

        // m_LightingButton.onClick.AddListener(() =>
        // {
        //     UIMgr.S.OpenPanel(UIID.TipsPanel, TIPSTYPE.Lighting);
        // });
        // m_SettingButton.onClick.AddListener(() =>
        // {
        //     UIMgr.S.OpenPanel(UIID.TipsPanel, TIPSTYPE.Lighting);
        // });

        // m_CoinsRedeemOpenBtn.onClick.AddListener(() =>
        // {
        //     UIMgr.S.OpenPanel(UIID.RedeemPanel);
        // });

        // m_DolloarRedeemOpenBtn.onClick.AddListener(() =>
        // {
        //     UIMgr.S.OpenPanel(UIID.RedeemPanel);
        // });
        // m_AmazonsRedeemOpenBtn.onClick.AddListener(() =>
        // {
        //     UIMgr.S.OpenPanel(UIID.RedeemPanel);
        // });
        // OnTokensCountUpdate(0);

    }

    protected override void OnOpen()
    {
        RegisterEvent(EventID.OnDolloarCountUpdate, OnDolloarCountUpdate);
        RegisterEvent(EventID.OnCoinsCountUpdate, OnCoinsCountUpdate);
        RegisterEvent(EventID.OnTokensCountUpdate, OnTokensCountUpdate);
        RegisterEvent(EventID.OnObstacleClearStateChange, OnObstacleClearStateChange);
        RegisterEvent(EventID.OnLightingActive, OnLightingActive);
    }

    private void OnLightingActive(int key, params object[] param)
    {
        //处理十秒钟的倒计时遮罩
        m_WallBtn.interactable = false;
        StartCoroutine(StartCoolingDown(10));
       
    }

    IEnumerator StartCoolingDown(int times) 
    {
        m_LightingCoolingDownLabel.gameObject.SetActive(true);
        while (times >= 1) 
        {
            m_LightingCoolingDownLabel.text = times.ToString();
            yield return new WaitForSeconds(1);
            --times;
        }

        m_WallBtn.interactable = m_WallBtn.enabled = true;
        m_LightingCoolingDownLabel.gameObject.SetActive(false);
    }

    private void OnObstacleClearStateChange(int key, params object[] param)
    {
        if (param == null || param.Length <= 0) 
        {
            return;
        }

        bool state = (bool)param[0];

        m_ShakeBtn.interactable = state;

        m_ClearCountDownTrans.gameObject.SetActive(!state);

        StartCoroutine(StartClearCoolingDown(Times)); 

/*        if (m_TimesID > -1) 
        {
            Timer.S.Cancel(m_TimesID);
            m_TimesID = Timer.S.Post2Scale(v =>
            {
                m_ClearCountDownLabel.text = (Times - v).ToString();
                if (v >= Times) 
                {
                    
                }

            }, 1, Times);
        }*/
    }

    IEnumerator StartClearCoolingDown(int times)
    {
        //EventSystem.S.Send(EventID.OnObstacleClearStateChange, false);
        m_ClearCountDownTrans.gameObject.SetActive(true);
        while (times >= 1)
        {
            m_ClearCountDownLabel.text = times.ToString();
            yield return new WaitForSeconds(1);
            --times;
        }
        EventSystem.S.Send(EventID.OnObstacleClearStateChange, true);
        m_ShakeBtn.interactable = m_ShakeBtn.enabled = true;
        m_ClearCountDownTrans.gameObject.SetActive(false);
    }

    private void OnDolloarCountUpdate(int key, params object[] param)
    {
        m_DolloarLabel.text = GameInfoMgr.data.GetDollar().ToString();
    }

    private void OnCoinsCountUpdate(int key, params object[] param)
    {
        m_CoinsLabel.text = GameInfoMgr.data.GetCoinsCount().ToString();
    }

    private void OnTokensCountUpdate(int key, params object[] param)
    {
        m_TokensLabel.text = GameInfoMgr.data.GetTokenCount().ToString();

        if (GameInfoMgr.data.GetTokenCount() >= 40)
        {
            m_NextInTimeLabel.gameObject.SetActive(false);
        }
        else 
        {
            m_NextInTimeLabel.gameObject.SetActive(true);

            if (m_TimesID > -1) 
            {
                return;
                Timer.S.Cancel(m_TimesID);
                m_TimesID = -1;
            }

            m_TimesID = Timer.S.Post2Scale(v =>
            {
                if (v >= 60) 
                {
                    m_TimesID = -1;
                    GameInfoMgr.data.AddToken(1);

                    AudioMgr.S.PlaySoundShoot("coinrecover");
                    
                }
                m_NextInTimeLabel.text = "nextin:" + (60 - v).ToString();

            },1, 60);
        }
    }
}



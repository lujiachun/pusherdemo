﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;
using System;

namespace GameWish.Game
{
    public class UIBaseCtrller : MonoBehaviour
    {
        protected AbstractPanel m_HoldingPanel;

        public virtual void OnInit(AbstractPanel panel)
        {
            m_HoldingPanel = panel;
        }

        public virtual void OnOpen(params object[] args)
        {

        }

        public virtual void OnClose()
        {

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{

    public class RedeemTipsPanel : AbstractPanel
    {
        [SerializeField]
        private Button m_CloseBtn;
        [SerializeField]
        private Text m_DescriptionLabel;

        protected override void OnUIInit()
        {
            m_CloseBtn.onClick.AddListener(CloseSelfPanel);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{

    public class SorryPanel : AbstractPanel
    {
        [SerializeField]
        private Button m_CloseBtn;
        [SerializeField]
        private Button m_OkButton;

        [SerializeField]
        private Text m_BehindLabel;
        [SerializeField]
        private Text m_RemianingCountLabel;
        [SerializeField]
        private Image m_ItemIcon;
        [SerializeField]
        private Text m_NoImageLabel;

        protected override void OnUIInit()
        {
            m_CloseBtn.onClick.AddListener(CloseSelfPanel);
            m_OkButton.onClick.AddListener(CloseSelfPanel);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            if (args == null || args.Length <= 0) 
            {
                CloseSelfPanel();
                return;
            }

            var data = args[0] as TDRedeemReward;

            if (data != null) 
            {
                if (string.IsNullOrEmpty(data.sprite))
                {
                    m_ItemIcon.gameObject.SetActive(false);
                    m_NoImageLabel.gameObject.SetActive(true);
                    m_NoImageLabel.text = TDLanguageTable.Get(data.unFinishLabel);
                }
                else 
                {
                    m_ItemIcon.gameObject.SetActive(true);
                    m_ItemIcon.sprite = FindSprite(data.sprite ,true);
                    m_NoImageLabel.gameObject.SetActive(false);
                    m_BehindLabel.text = TDLanguageTable.Get(data.unFinishLabel); 
                }

                //TODO 读表获取内容
                
            }
            //  
        }
    }
}
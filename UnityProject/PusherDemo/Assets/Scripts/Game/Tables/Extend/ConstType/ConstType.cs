﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public enum ConstType
    {
        SOUND_DEFAULT_BUTTON = 0,
        SOUND_MAINBGM,
        GAME_TIP_COST,
        SUPPLY_AD_REWARD_COINS,
        SOUND_GAIN_COIN,
        SOUND_CANCEL,
        DEFAULT_DAILY_COIN,
        DEFAULT_DAILY_DOUBLE,
        MAX
    }
}

using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public partial class TDLevelRatio
    {
        public void Reset()
        {

        }

        public int GetRatioNumByStage(int stage) 
        {
            string filedName = "levelStage" + stage;
            return int.Parse(this.GetType().GetProperty(filedName).GetValue(this).ToString());
        }


    }
}
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public partial class TDMultipeRatioTable
    {
        static void CompleteRowAdd(TDMultipeRatio tdData)
        {
            
        }

        public static bool IsCanUseCurrentMulti(int mul)
        {
            if (mul == 1)
            {
                return true;
            }

            int id = (int)GameInfoMgr.data.GetDollar() / 10;

            var config = GetData(id + 1);

            if (mul == 2) 
            {
                return config.doubleRate > GameInfoMgr.data.m_2Count;
            }

            if (mul == 5)
            {
                return config.fiveRate > GameInfoMgr.data.m_5Count;
            }

            if (mul == 10)
            {
                return config.tenRate > GameInfoMgr.data.m_10Count;
            }

            if (mul == 20)
            {
                return config.twentyRate > GameInfoMgr.data.m_20Count;
            }

            return false;
        }
    }
}
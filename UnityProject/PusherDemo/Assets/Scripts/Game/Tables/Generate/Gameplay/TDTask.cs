//Auto Generate Don't Edit it
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public partial class TDTask
    {
        
       
        private EInt m_Id = 0;   
        private string m_Sprite;   
        private EInt m_NeedCount = 0;   
        private string m_ReardType;  
        
        //private Dictionary<string, TDUniversally.FieldData> m_DataCacheNoGenerate = new Dictionary<string, TDUniversally.FieldData>();
      
        /// <summary>
        /// ID
        /// </summary>
        public  int  id {get { return m_Id; } }
       
        /// <summary>
        /// SpriteName图标名字
        /// </summary>
        public  string  sprite {get { return m_Sprite; } }
       
        /// <summary>
        /// 奖励纪律（对应表ID）
        /// </summary>
        public  int  needCount {get { return m_NeedCount; } }
       
        /// <summary>
        /// 奖励类型（枚举）
        /// </summary>
        public  string  reardType {get { return m_ReardType; } }
       

        public void ReadRow(DataStreamReader dataR, int[] filedIndex)
        {
          //var schemeNames = dataR.GetSchemeName();
          int col = 0;
          while(true)
          {
            col = dataR.MoreFieldOnRow();
            if (col == -1)
            {
              break;
            }
            switch (filedIndex[col])
            { 
            
                case 0:
                    m_Id = dataR.ReadInt();
                    break;
                case 1:
                    m_Sprite = dataR.ReadString();
                    break;
                case 2:
                    m_NeedCount = dataR.ReadInt();
                    break;
                case 3:
                    m_ReardType = dataR.ReadString();
                    break;
                default:
                    //TableHelper.CacheNewField(dataR, schemeNames[col], m_DataCacheNoGenerate);
                    break;
            }
          }

        }
        
        public static Dictionary<string, int> GetFieldHeadIndex()
        {
          Dictionary<string, int> ret = new Dictionary<string, int>(4);
          
          ret.Add("Id", 0);
          ret.Add("Sprite", 1);
          ret.Add("NeedCount", 2);
          ret.Add("ReardType", 3);
          return ret;
        }
    } 
}//namespace LR
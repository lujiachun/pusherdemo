//Auto Generate Don't Edit it
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public partial class TDMultipeRatio
    {
        
       
        private EInt m_ID = 0;   
        private EInt m_DoubleRate = 0;   
        private EInt m_FiveRate = 0;   
        private EInt m_TenRate = 0;   
        private EInt m_TwentyRate = 0;  
        
        //private Dictionary<string, TDUniversally.FieldData> m_DataCacheNoGenerate = new Dictionary<string, TDUniversally.FieldData>();
      
        /// <summary>
        /// ID
        /// </summary>
        public  int  iD {get { return m_ID; } }
       
        /// <summary>
        /// 2
        /// </summary>
        public  int  doubleRate {get { return m_DoubleRate; } }
       
        /// <summary>
        /// 5
        /// </summary>
        public  int  fiveRate {get { return m_FiveRate; } }
       
        /// <summary>
        /// 10
        /// </summary>
        public  int  tenRate {get { return m_TenRate; } }
       
        /// <summary>
        /// 20
        /// </summary>
        public  int  twentyRate {get { return m_TwentyRate; } }
       

        public void ReadRow(DataStreamReader dataR, int[] filedIndex)
        {
          //var schemeNames = dataR.GetSchemeName();
          int col = 0;
          while(true)
          {
            col = dataR.MoreFieldOnRow();
            if (col == -1)
            {
              break;
            }
            switch (filedIndex[col])
            { 
            
                case 0:
                    m_ID = dataR.ReadInt();
                    break;
                case 1:
                    m_DoubleRate = dataR.ReadInt();
                    break;
                case 2:
                    m_FiveRate = dataR.ReadInt();
                    break;
                case 3:
                    m_TenRate = dataR.ReadInt();
                    break;
                case 4:
                    m_TwentyRate = dataR.ReadInt();
                    break;
                default:
                    //TableHelper.CacheNewField(dataR, schemeNames[col], m_DataCacheNoGenerate);
                    break;
            }
          }

        }
        
        public static Dictionary<string, int> GetFieldHeadIndex()
        {
          Dictionary<string, int> ret = new Dictionary<string, int>(5);
          
          ret.Add("ID", 0);
          ret.Add("DoubleRate", 1);
          ret.Add("FiveRate", 2);
          ret.Add("TenRate", 3);
          ret.Add("TwentyRate", 4);
          return ret;
        }
    } 
}//namespace LR
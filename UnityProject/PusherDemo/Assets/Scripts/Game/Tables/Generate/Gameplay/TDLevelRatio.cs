//Auto Generate Don't Edit it
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public partial class TDLevelRatio
    {
        
       
        private EInt m_Id = 0;   
        private string m_LevelStage1;   
        private string m_LevelStage2;   
        private string m_LevelStage3;   
        private string m_LevelStage4;   
        private string m_LevelStage5;   
        private string m_LevelStage6;   
        private string m_LevelStage7;   
        private string m_LevelStage8;   
        private string m_LevelStage9;   
        private string m_LevelStage10;  
        
        //private Dictionary<string, TDUniversally.FieldData> m_DataCacheNoGenerate = new Dictionary<string, TDUniversally.FieldData>();
      
        /// <summary>
        /// ID
        /// </summary>
        public  int  id {get { return m_Id; } }
       
        /// <summary>
        /// Name
        /// </summary>
        public  string  levelStage1 {get { return m_LevelStage1; } }
       
        /// <summary>
        /// SpriteName图标名字
        /// </summary>
        public  string  levelStage2 {get { return m_LevelStage2; } }
       
        /// <summary>
        /// 奖励类型（枚举）
        /// </summary>
        public  string  levelStage3 {get { return m_LevelStage3; } }
       
        /// <summary>
        /// Name
        /// </summary>
        public  string  levelStage4 {get { return m_LevelStage4; } }
       
        /// <summary>
        /// SpriteName图标名字
        /// </summary>
        public  string  levelStage5 {get { return m_LevelStage5; } }
       
        /// <summary>
        /// 奖励类型（枚举）
        /// </summary>
        public  string  levelStage6 {get { return m_LevelStage6; } }
       
        /// <summary>
        /// Name
        /// </summary>
        public  string  levelStage7 {get { return m_LevelStage7; } }
       
        /// <summary>
        /// SpriteName图标名字
        /// </summary>
        public  string  levelStage8 {get { return m_LevelStage8; } }
       
        /// <summary>
        /// 奖励类型（枚举）
        /// </summary>
        public  string  levelStage9 {get { return m_LevelStage9; } }
       
        /// <summary>
        /// Name
        /// </summary>
        public  string  levelStage10 {get { return m_LevelStage10; } }
       

        public void ReadRow(DataStreamReader dataR, int[] filedIndex)
        {
          //var schemeNames = dataR.GetSchemeName();
          int col = 0;
          while(true)
          {
            col = dataR.MoreFieldOnRow();
            if (col == -1)
            {
              break;
            }
            switch (filedIndex[col])
            { 
            
                case 0:
                    m_Id = dataR.ReadInt();
                    break;
                case 1:
                    m_LevelStage1 = dataR.ReadString();
                    break;
                case 2:
                    m_LevelStage2 = dataR.ReadString();
                    break;
                case 3:
                    m_LevelStage3 = dataR.ReadString();
                    break;
                case 4:
                    m_LevelStage4 = dataR.ReadString();
                    break;
                case 5:
                    m_LevelStage5 = dataR.ReadString();
                    break;
                case 6:
                    m_LevelStage6 = dataR.ReadString();
                    break;
                case 7:
                    m_LevelStage7 = dataR.ReadString();
                    break;
                case 8:
                    m_LevelStage8 = dataR.ReadString();
                    break;
                case 9:
                    m_LevelStage9 = dataR.ReadString();
                    break;
                case 10:
                    m_LevelStage10 = dataR.ReadString();
                    break;
                default:
                    //TableHelper.CacheNewField(dataR, schemeNames[col], m_DataCacheNoGenerate);
                    break;
            }
          }

        }
        
        public static Dictionary<string, int> GetFieldHeadIndex()
        {
          Dictionary<string, int> ret = new Dictionary<string, int>(11);
          
          ret.Add("Id", 0);
          ret.Add("LevelStage1", 1);
          ret.Add("LevelStage2", 2);
          ret.Add("LevelStage3", 3);
          ret.Add("LevelStage4", 4);
          ret.Add("LevelStage5", 5);
          ret.Add("LevelStage6", 6);
          ret.Add("LevelStage7", 7);
          ret.Add("LevelStage8", 8);
          ret.Add("LevelStage9", 9);
          ret.Add("LevelStage10", 10);
          return ret;
        }
    } 
}//namespace LR
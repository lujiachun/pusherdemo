﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Qarth;
using System.Reflection;
using UnityEngine.Purchasing;

namespace GameWish.Game
{
    public class PurchaseModule : TSingleton<PurchaseModule>
    {
        private Type m_PurchaseServiceType;

        public Type purchaseServiceType
        {
            get
            {
                if (m_PurchaseServiceType == null)
                {
                    m_PurchaseServiceType = Type.GetType("GameWish.Game.PurchaseModule");
                }

                return m_PurchaseServiceType;
            }
        }

        public void Init()
        {
            EventSystem.S.Register(SDKEventID.OnPurchaseSuccess, OnPurchaseSuccess);
            PurchaseMgr.S.Init();
            PurchaseMgr.S.InitPurchaseInfo();
        }

        protected void OnPurchaseSuccess(int key, params object[] args)
        {
            TDPurchase data = args[0] as TDPurchase;
            Product product = args[1] as Product;
            
            bool isFirstBuy = product.definition.payouts.Count() == 1;
            
               // PlayerInfoMgr.data.currencyData.Add(Enum_CurrencyType.Diamond, count);
           
        }

        public string GetPriceText(TDPurchase data)
        {
            if (string.IsNullOrEmpty(data.localPriceString))
            {
                return !data.price.Equals(0) ? string.Format("${0}", data.price / 100f) : "";
                //TDLanguageTable.GetFormat("UI_PURCHASETITLE", m_Data.price);           
            }
            else
            {
                return data.localPriceString;
            }
        }

        protected void ProcessPurchaseService(TDPurchase data)
        {
            if (data == null || string.IsNullOrEmpty(data.serviceKey))
            {
                return;
            }

            Type type = purchaseServiceType;

            if (type == null)
            {
                return;
            }

            MethodInfo servicemethod = type.GetMethod(data.serviceKey);

            if (servicemethod == null)
            {
                Log.e("Invalid Purchase Service Name:" + data.serviceKey);
                return;
            }

            try
            {
                servicemethod.Invoke(null, new object[] { data });
            }
            catch (Exception e)
            {
                Log.e(e);
            }
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{
    public class MyGuideWordsCommand : AbstractGuideCommand
    {
        private bool m_NeedClose = true;
        private object[] m_Params;

        public override void SetParam(object[] pv)
        {
            if (pv.Length == 0)
            {
                Log.w("MyGuideWordsCommand Init With Invalid Param.");
                return;
            }
            m_NeedClose = Helper.String2Bool(pv[0].ToString());
            m_Params = pv;
        }

        protected override void OnStart()
        {
            if (m_Params.Length > 4)
            {
                switch (m_Params[4])
                {
/*                    case "Top":
                        UIMgr.S.OpenTopPanel(UIID.MyGuideWordsPanel, null, m_Params);
                        break;
                    case "Bottom":
                        UIMgr.S.OpenBottomPanel(UIID.MyGuideWordsPanel, null, m_Params);
                        break;*/
                }
            }
            else
            {
                //UIMgr.S.OpenTopPanel(UIID.MyGuideWordsPanel, null, m_Params);
            }
        }

        protected override void OnFinish(bool forceClean)
        {
            if (m_NeedClose || forceClean)
            {
/*                var panel = UIMgr.S.FindPanel(UIID.MyGuideWordsPanel);
                if (panel != null)
                {
                    (panel as AbstractAnimPanel).HideSelfWithAnim();
                }*/
            }
        }
    }
}

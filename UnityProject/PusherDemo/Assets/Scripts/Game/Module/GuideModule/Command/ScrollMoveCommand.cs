using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{
    public class ScrollMoveCommand : AbstractGuideCommand
    {
        private IUINodeFinder m_Finder;
        private float m_Val;

        public override void SetParam(object[] pv)
        {
            if (pv != null && pv.Length > 0)
            {
                m_Finder = pv[0] as IUINodeFinder;

                if (pv.Length > 1)
                {
                    m_Val = Helper.String2Float(pv[1].ToString());
                }
            }
        }

        protected override void OnStart()
        {
            RectTransform targetNode = m_Finder.FindNode(false) as RectTransform;
            if (targetNode == null)
                return;

            var rect = targetNode.GetComponent<ScrollRect>();
            if (rect == null)
                return;

            rect.DoScrollVertical(m_Val, 0.5f);
        }

        protected override void OnFinish(bool forceClean)
        {

        }
    }
}

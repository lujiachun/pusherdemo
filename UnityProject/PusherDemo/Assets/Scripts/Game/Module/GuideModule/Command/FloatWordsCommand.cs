using System.Collections;
using System.Collections.Generic;
using System;
using Qarth;
using UnityEngine;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class FloatWordsCommand : AbstractGuideCommand
    {
        private string m_Word;

        public override void SetParam(object[] pv)
        {
            if (pv.Length == 0)
            {
                Log.w("Init With Invalid Param.");
                return;
            }

            try
            {
                m_Word = pv[0].ToString();
            }
            catch (Exception e)
            {
                Log.e(e);
            }
        }
        protected override void OnStart()
        {
            FloatMessage.S.ShowMsg(TDLanguageTable.Get(m_Word));
            FinishStep();
        }
    }
}


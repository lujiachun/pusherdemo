using System;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
    public class RefreshPauseCommand : AbstractGuideCommand
    {
        private bool m_Status = true;

        public override void SetParam(object[] param)
        {
            if (param.Length > 0)
                m_Status = Helper.String2Bool((string)param[0]);
        }

        protected override void OnStart()
        {
            EventSystem.S.Send(EventID.OnRefreshPauseStatus, m_Status);
        }

        protected override void OnFinish(bool forceClean)
        {

        }
    }
}


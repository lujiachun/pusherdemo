﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class HighlightUIsCommand : AbstractGuideCommand
    {
        private List<IUINodeFinder> m_Finders = new List<IUINodeFinder>();
        private bool m_NeedClose = true;
        private List<Canvas> m_LstCanvas;
        private List<GraphicRaycaster> m_LstRayCaster = new List<GraphicRaycaster>();

        private Canvas m_Canvas;
        private int m_CanvasPreSortingOrder = -1;
        private GraphicRaycaster m_GraphicRaycaster;
        private bool m_IsCreateGraphicRaycaster;
        private int m_PanelOrder;

        public override void SetParam(object[] pv)
        {
            if (pv.Length == 0)
            {
                Log.w("HighlightUICommand Init With Invalid Param.");
                return;
            }

            for (int i = 0; i < pv.Length; i++)
            {
                m_Finders.Add(pv[i] as IUINodeFinder);
            }
        }

        protected override void OnStart()
        {
            m_LstCanvas = new List<Canvas>();
            RectTransform targetNode = null;
            for (int i = 0; i < m_Finders.Count; i++)
            {
                targetNode = m_Finders[i].FindNode(false) as RectTransform;
                if (targetNode == null)
                {
                    return;
                }
                m_Canvas = targetNode.GetComponent<Canvas>();
                if (m_Canvas == null)
                {
                    m_Canvas = targetNode.gameObject.AddComponent<Canvas>();
                    m_Canvas.overrideSorting = true;
                    m_CanvasPreSortingOrder = -1;

                }
                else
                {
                    m_CanvasPreSortingOrder = m_Canvas.sortingOrder;
                }
                m_LstCanvas.Add(m_Canvas);

                m_GraphicRaycaster = targetNode.GetComponent<GraphicRaycaster>();
                if (m_GraphicRaycaster == null)
                {
                    m_IsCreateGraphicRaycaster = true;
                    m_GraphicRaycaster = targetNode.gameObject.AddComponent<GraphicRaycaster>();

                    var panel = targetNode.GetComponentInParent<AbstractPanel>();
                    if (panel != null)
                    {
                        UIMgr.S.SetPanelStateDirtyByPanelId(panel.GetParentPanelID());
                    }
                }

                m_LstRayCaster.Add(m_GraphicRaycaster);
            }

            Action<int> orderUpdate = OnSortingOrderUpdate;
            UIMgr.S.OpenTopPanel(EngineUI.HighlightMaskPanel, null, orderUpdate);
        }

        protected void OnSortingOrderUpdate(int panelOrder)
        {
            if (m_LstCanvas != null)
            {
                for (int i = 0; i < m_LstCanvas.Count; i++)
                {
                    m_PanelOrder = panelOrder;
                    m_LstCanvas[i].overrideSorting = true;
                    m_LstCanvas[i].sortingOrder = m_PanelOrder + 1;
                }
                Timer.S.Post2Scale(OnTimeReach, 0.1f);
            }
        }

        private void OnTimeReach(int count)
        {
            if (m_LstCanvas != null)
            {
                for (int i = 0; i < m_LstCanvas.Count; i++)
                {
                    m_LstCanvas[i].overrideSorting = true;
                    m_LstCanvas[i].sortingOrder = m_PanelOrder + 1;
                }
                Timer.S.Post2Scale(OnTimeReach, 0.1f);

            }
        }

        protected override void OnFinish(bool forceClean)
        {
            if (m_NeedClose || forceClean)
            {
                UIMgr.S.ClosePanelAsUIID(EngineUI.HighlightMaskPanel);
            }

            for (int i = 0; i < m_LstRayCaster.Count; i++)
            {
                GameObject.Destroy(m_LstRayCaster[i]);
            }
            m_LstRayCaster.Clear();

            if (m_LstCanvas != null)
            {
                for (int i = 0; i < m_LstCanvas.Count; i++)
                {
                    if (m_CanvasPreSortingOrder >= 0)
                    {
                        m_LstCanvas[i].sortingOrder = m_CanvasPreSortingOrder;
                    }
                    else
                    {
                        GameObject.Destroy(m_LstCanvas[i]);
                    }
                }

                m_LstCanvas = null;
            }
        }
    }
}


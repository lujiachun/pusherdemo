﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class UICloseTrigger : ITrigger
    {
        private Action<bool, ITrigger> m_Listener;
        private string m_PanelName;
        private string m_TempName;

        public void SetParam(object[] param)
        {
            m_PanelName = param[0].ToString();
        }

        public void Start(Action<bool, ITrigger> l)
        {
            m_Listener = l;
            EventSystem.S.Register(EventID.OnPanelClose, OnPanelClose);

            OnPanelClose(0);
        }

        public void Stop()
        {
            m_Listener = null;
            EventSystem.S.UnRegister(EventID.OnPanelClose, OnPanelClose);
        }

        public bool isReady
        {
            get
            {
                return string.IsNullOrEmpty(m_TempName) ? false : m_PanelName == m_TempName ? true : false;
            }
        }

        private void OnPanelClose(int key, params object[] args)
        {
            if (m_Listener == null)
            {
                return;
            }
            if (args.Length > 0)
                m_TempName = args[0].ToString();
            if (isReady)
            {
                m_Listener(true, this);
            }
            else
            {
                //Log.w ("Not m_Finder UINode:" + m_Finder.ToString());
                m_Listener(false, this);
            }
        }
    }
}


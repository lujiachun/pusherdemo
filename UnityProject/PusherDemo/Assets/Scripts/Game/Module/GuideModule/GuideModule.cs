﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game
{
    public class GuideModule : AbstractModule
    {
        public void StartGuide()
        {
            if (!AppConfig.S.isGuideActive)
            {
                return;
            }
            InitCustomTrigger();
            InitCustomCommand();

            GuideMgr.S.StartGuideTrack();
        }

        protected override void OnComAwake()
        {

        }

        protected void InitCustomTrigger()
        {
            GuideMgr.S.RegisterGuideTrigger(typeof(UINodeInvisibleTrigger));
            GuideMgr.S.RegisterGuideTrigger(typeof(UICloseTrigger));
        }

        protected void InitCustomCommand()
        {
            GuideMgr.S.RegisterGuideCommand(typeof(WaitCommand));
            GuideMgr.S.RegisterGuideCommand(typeof(HighlightUIsCommand));
            GuideMgr.S.RegisterGuideCommand(typeof(ButtonDragCommand));
            GuideMgr.S.RegisterGuideCommand(typeof(CharaWordsCommand));
            GuideMgr.S.RegisterGuideCommand(typeof(FloatWordsCommand));
            GuideMgr.S.RegisterGuideCommand(typeof(OpenTopPanelCommand));
            GuideMgr.S.RegisterGuideCommand(typeof(RefreshPauseCommand));
            GuideMgr.S.RegisterGuideCommand(typeof(MyGuideWordsCommand));
            GuideMgr.S.RegisterGuideCommand(typeof(MyButtonHackCommand));

            // GuideMgr.S.RegisterGuideCommand(typeof(CharaWordsCommand));
            // GuideMgr.S.RegisterGuideCommand(typeof(ShowGuideLineCommand));
        }
    }
}

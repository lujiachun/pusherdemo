﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine.SceneManagement;

namespace GameWish.Game
{
    public class InputModule : AbstractModule
    {
        private IInputter m_KeyboardInputter;
        private KeyCodeTracker m_KeyCodeTracker;

        public override void OnComLateUpdate(float dt)
        {
            m_KeyboardInputter.LateUpdate();
            m_KeyCodeTracker.LateUpdate();
        }

        protected override void OnComAwake()
        {
            m_KeyCodeTracker = new KeyCodeTracker();
            m_KeyCodeTracker.SetDefaultProcessListener(ShowBackKeydownTips);

            m_KeyboardInputter = new KeyboardInputter();
            m_KeyboardInputter.RegisterKeyCodeMonitor(KeyCode.F1, null, OnClickF1, null);
            m_KeyboardInputter.RegisterKeyCodeMonitor(KeyCode.F2, null, OnClickF2, null);
            m_KeyboardInputter.RegisterKeyCodeMonitor(KeyCode.F3, null, OnClickF3, null);
            m_KeyboardInputter.RegisterKeyCodeMonitor(KeyCode.F4, null, OnClickF4, null);
            m_KeyboardInputter.RegisterKeyCodeMonitor(KeyCode.F5, null, OnClickF5, null);
            m_KeyboardInputter.RegisterKeyCodeMonitor(KeyCode.F6, null, OnClickF6, null);
        }

        private void ShowBackKeydownTips()
        {

        }

        private void OnClickF1()
        {
            EventSystem.S.Send(EventID.OnWallOpen);
        }

        private void OnClickF2()
        {

        }

        private void OnClickF3()
        {
            EventSystem.S.Send(EventID.OnSlotReward, SlotRewardType.Prize777);
        }

        private void OnClickF4()
        {
            EventSystem.S.Send(EventID.OnRollSlotMachine);
        }

        private void OnClickF5()
        {


        }

        private void OnClickF6()
        {
            EventSystem.S.Send(EventID.OnDoubleModeStateChange, true);
        }

        private void OnSceneLoadResult(string sceneName, bool result)
        {
            Log.i("SceneLoad:" + sceneName + " " + result);
        }
    }
}

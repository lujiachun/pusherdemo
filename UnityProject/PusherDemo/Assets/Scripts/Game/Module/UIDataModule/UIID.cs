﻿using UnityEngine;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    public enum UIID
    {
        LogoPanel = 0,
        SplashPanel,
        GuideWordsPanel,
        LoadingPanel,
        WorldUIPanel,
        GamingPanel,
        GameWinPanel,
        GameLosePanel,
        GamePausePanel,
        GameLoseTipsPanel,

        ShopPanel,
        SettingPanel,
        MainPanel,
        BattleHintPanel,
        RulePanel,

        SignInPanel,


        OfflinePanel,
        StaminaPanel,



        DecorationPanel,
        TipsPanel,
        SlotMachinePanel,
        SlotFailedPanel,
        TokensNotEnoughPanel,
        SorryPanel,
        RedeemPanel,
        RedeemWithdrawPanel,
        
        WallPanel,
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameWish.Game
{
    public class FriendAttackState : StateBase
    {
        private StateEntity m_Entity;
        
        public FriendAttackState(StateMachineBase machine, StateEntity entity) : base(machine, entity)
        {
            m_Entity = entity;
        }

        public override void OnStateIn()
        {
            m_Entity.Attack();
        }

        public override void OnStateUpdate()
        {
            
        }

        public override void OnStateOut()
        {
            
        }
    }

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameWish.Game
{
    public class FriendIdleState : StateBase
    {
        private StateEntity m_Entity;
        
        public FriendIdleState(StateMachineBase machine, StateEntity entity) : base(machine, entity)
        {
            m_Entity = entity;
        }

        public override void OnStateIn()
        {
            m_Entity.Idle();
        }

        public override void OnStateUpdate()
        {
            
        }

        public override void OnStateOut()
        {
            
        }
    }

}


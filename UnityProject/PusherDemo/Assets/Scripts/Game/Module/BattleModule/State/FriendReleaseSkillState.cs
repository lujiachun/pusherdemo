﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameWish.Game
{
    public class FriendReleaseSkillState : StateBase
    {
        private StateEntity m_Entity;
        
        public FriendReleaseSkillState(StateMachineBase machine, StateEntity entity) : base(machine, entity)
        {
            m_Entity = entity;
        }

        public override void OnStateIn()
        {
            m_Entity.DoSkill();
        }

        public override void OnStateUpdate()
        {
            
        }

        public override void OnStateOut()
        {
            
        }
    }

}


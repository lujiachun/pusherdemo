﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameWish.Game
{
    public class FriendWalkState : StateBase
    {
        private StateEntity m_Entity;
        
        public FriendWalkState(StateMachineBase machine, StateEntity entity) : base(machine, entity)
        {
            m_Entity = entity;
        }

        public override void OnStateIn()
        {
            m_Entity.Walk();
        }

        public override void OnStateUpdate()
        {
            
        }

        public override void OnStateOut()
        {
            
        }
    }

}


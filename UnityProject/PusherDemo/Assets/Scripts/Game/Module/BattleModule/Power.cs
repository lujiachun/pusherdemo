using System;
using System.Collections.Generic;

using UnityEngine;

public class Power : MonoBehaviour
{
    private Vector3 m_endPos;
    public void SetEndPos(Vector3 pos)
    {
        m_endPos = pos;
    }
    public Vector3 GetEndPos()
    {
        return m_endPos;
    }
}

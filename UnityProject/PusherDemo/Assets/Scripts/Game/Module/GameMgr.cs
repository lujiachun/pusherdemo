﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Qarth;
using System;

namespace GameWish.Game
{
    [TMonoSingletonAttribute("[Game]/GameMgr")]
    public class GameMgr : AbstractModuleMgr, ISingleton
    {
        private static GameMgr s_Instance;
        private int m_GameplayInitSchedule = 0;

        public static GameMgr S
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = MonoSingleton.CreateMonoSingleton<GameMgr>();
                }
                return s_Instance;
            }
        }

        public void InitGameMgr()
        {
            Log.i("Init[GameMgr]");
            AppropriateLongScreen();
        }

        public void OnSingletonInit()
        {

        }

        protected override void OnActorAwake()
        {
            ShowLogoPanel();
        }

        protected override void OnActorStart()
        {
            StartProcessModule module = AddMonoCom<StartProcessModule>();
            module.SetFinishListener(OnStartProcessFinish);
        }

        protected void ShowLogoPanel()
        {
            UIDataModule.RegisterStaticPanel();

            Action a = OnLogoPanelFinish;
            UIMgr.S.OpenTopPanel(UIID.LogoPanel, null, a);
            
        }

        protected void OnLogoPanelFinish()
        {
            ++m_GameplayInitSchedule;
            TryStartGameplay();
        }

        protected void OnStartProcessFinish()
        {
            ++m_GameplayInitSchedule;
            TryStartGameplay();
        }

        protected void TryStartGameplay()
        {
            if (m_GameplayInitSchedule < 2)
            {
                return;
            }
            
            //RealNameRemoteConfMgr.S.Init(CreditHttpConfig.remoteConfUrl, CreditHttpConfig.remoteConfAppName);
            GetCom<GuideModule>().StartGuide();
            GameplayMgr.S.InitGameplay();
            
            AdsMgr.S.PreloadAllAd();
            PurchaseModule.S.Init();
            //RemoteConfigMgr.S.StartChecker(null);
        }

        // private void OnApplicationFocus(bool focusStatus)
        // {
        //     if (!focusStatus)
        //     {
        //         CreditMgr.S.RegisterNotificate(DateTime.Today.AddHours(11).AddMinutes(30), DateTime.Today.AddHours(20).AddMinutes(30));
        //     }
        // }

        // private void OnApplicationPause(bool pauseStatus)
        // {
        //     if (pauseStatus)
        //     {
        //         CreditMgr.S.RegisterNotificate(DateTime.Today.AddHours(11).AddMinutes(30), DateTime.Today.AddHours(20).AddMinutes(30));
        //     }
        // }

        private void OnApplicationQuit()
        {
        }

        void AppropriateLongScreen()
        {
            if (Screen.height * 1.0f / Screen.width > 2)
            {
                var rectRoot = UIMgr.S.uiRoot.panelRoot.GetComponent<RectTransform>();
                rectRoot.offsetMax = new Vector2(rectRoot.offsetMax.x, -Define.LONG_SCREEN_OFFSET_TOP);
#if UNITY_IOS
                rectRoot.offsetMin = new Vector2(rectRoot.offsetMin.x, Define.LONG_SCREEN_OFFSET_BOTTOM);
#endif
            }
        }
    }
}

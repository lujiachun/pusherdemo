﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public class Define
    {
        public const string FIRST_BONUS = "first_bonus";
        public const string SOUND_DEFAULT_SOUND = "Click";
        public const string SIGN_DAY_KEY = "Gameplay_StartDate";
        public const string HAS_RETENTION = "retention_flag_day1";
        public const int LONG_SCREEN_OFFSET_TOP = 60;
        public const int LONG_SCREEN_OFFSET_BOTTOM = 15;

        #region GameEvent
        public const string START_GAME = "StartGaming";

        public const string BUZZ_STATE = "Buzz_State";
        public const string SHAKE_STATE = "Shake_State";
        #endregion

        //event
        public const string EVT_SHARE = "ShareGameLink";

        //
        public const string SINGLR_TABLE = "SingleTable";
        //SPRITE  NAME 
        public const string AD_PLACEMENT_REWARD = "MainReward";
        public const string AD_PLACEMENT_INTER = "MainInter";
        public const string AD_PLACEMENT_LEVEL_MIXVIEW = "LevelMixView";
        public const string AD_PLACEMENT_INFO_MIXVIEW = "InfoMixView";
        public const string AD_PLACEMENT_FAIL_MIXVIEW = "FailMixView";
        public const string AD_PLACEMENT_RED_MIXVIEW = "RedMixView";
        public const string AD_PLACEMENT_FAKE_RED_MIXVIEW = "FakeRedMixView";
        public const string AD_PLACEMENT_FAKE_LEVEL_MIXVIEW = "FakeLevelMixView";
        public const string AD_PLACEMENT_ACT_MIXVIEW = "ActMixView";

        public class GameDefine
        {
            public const int MAX_TOKEN_LIMIT = 40;
            public const int SUPPLY_TIME = 300;
            public static int[] DOLLOAR_DAILY_LIMIT = new int[] { 10, 10, 10, 8, 8, 5, 1, 0, 0, 0 };
            public static int[] RATIO_MULTI_HOLE = new int[] { 50, 20, 15, 10, 5 };
            public static int[] MultiList = new int[] { 1, 2, 5, 10, 20 };
        }
    }
}

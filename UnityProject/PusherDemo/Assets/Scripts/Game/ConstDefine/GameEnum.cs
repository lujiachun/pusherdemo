﻿using UnityEngine;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    /// <summary>
    /// 小伙伴类型（属性）
    /// </summary>
    public enum Enum_FriendType
    {
        //水
        Water = 0,
        //木
        Wood = 1,
        //光
        Light = 2,
        //暗
        Dark = 3,
        //火
        Fire = 4
    }
    /// <summary>
    /// 奖励类型
    /// </summary>
    public enum Enum_RewardType
    {
        //金币
        Currency,
        //材料
        Material,
        //道具
        Prop,
        //经验
        Exp,
        //体力
        Stamina,
    }
    /// <summary>
    /// 货币类型
    /// </summary>
    public enum Enum_CurrencyType
    {
        //金币
        Coin,
        //钻石
        Diamond,
    }

    /// <summary>
    /// 小伙伴升级材料类型
    /// </summary>
    public enum Enum_MaterialType
    {
        //钥匙
        Key = 105,
        //普通
        Normal = 106,
        //特殊
        Special = 107,
        //矿石
        Ore = 108,

    }

    /// <summary>
    /// 任务与成就类型
    /// </summary>
    public enum Enum_TaskType
    {
        None,
        //签到
        CheckSign,
        //幸运转盘
        CheckDrawCount,
        //击杀怪物
        CheckHitMonster,
        //消除元素
        CheckClearBlock,
        //使用技能
        CheckUseSkill,
        //完成副本
        CheckCompleteFB,
        //通过最高关卡
        CheckPassMaxLv,
        //获得星星数量
        CheckGetStar,
        //伙伴数量
        CheckFriendsNum,
        //所有伙伴达到XX级
        CheckAllFriendsMaxLv,
        //有伙伴达到XX级
        CheckFriendsMaxLv,
        //累计获得钻石
        CheckGetDiamond,
        //累计获得金币
        CheckGetCoin,
        //累计开启N个原石
        CheckOpenStoneBox,
        //累计消耗钻石
        CheckUseDiamond,
        //累计获得N次转盘大奖
        CheckGetDrawBigReward,
        //累计开启N次星星宝箱
        CheckOpenStarBox,

    }

    /// <summary>
    /// 任务与成就子类型
    /// </summary>
    public enum Enum_TaskSubType
    {
        None,
        //随机类型
        Random,
        //指定id类型
        Specific,
    }

    /// <summary>
    /// 成就大类型
    /// </summary>
    public enum Enum_AchievementMainType
    {
        None,
        Fight = 1,
        Friends = 2,
        Fortune = 3,
        Social = 4,
        Activity = 5,
    }
    /// <summary>
    /// 家园建筑状态
    /// </summary>
    public enum Enum_HomelandBulidingState
    {
        None,//未解锁
        Normal,//正常状态
        Upgrade,//升级中（可以生产 无法收获）
        Limit,//生产达到上限
    }
}
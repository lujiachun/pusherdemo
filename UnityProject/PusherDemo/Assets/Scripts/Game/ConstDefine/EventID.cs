﻿using UnityEngine;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    public enum EventID
    {
        //引导
        OnPanelClose,
        OnCloseGuideHand,
        OnRefreshPauseStatus,
        OnRefreshLevelMapUIRaycast,


        OnLanguageTableSwitchFinish,

        OnQuitTriggerEnter,
        OnTokensCountUpdate,
        OnCoinsCountUpdate,
        OnDolloarCountUpdate,
        OnObstacleClearStateChange,
        OnLightingActive,
        OnProcessHalfGreen,
        OnRollSlotMachine,
        OnAmazonCountUpdate,
        OnRedeemRecordUpdate,
        OnSlotReward,
        AllocateDecoration,
        OnDoubleModeStateChange,
        OnTaskChange,
        // 墙的开闭
        OnWallOpen,
        OnWallClose,
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
namespace GameWish.Game
{
    public class PopBtn : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField]
        private Vector3 m_ClickDownScale = new Vector3(0.95f, 0.95f, 0.95f);
        [SerializeField]
        private Vector3 m_NormalScale = Vector3.one;
        float m_ScaleTime = 0.07f;

        public void OnPointerDown(PointerEventData eventData)
        {
            transform.DOKill();
            transform.DOScale(m_ClickDownScale, m_ScaleTime);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            transform.DOKill();
            transform.DOScale(m_NormalScale, m_ScaleTime);
        }

    }


}
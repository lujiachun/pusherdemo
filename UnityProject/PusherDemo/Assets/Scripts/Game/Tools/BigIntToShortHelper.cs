﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System.Text;
using System.Text.RegularExpressions;

namespace GameWish.Game
{


    public class BigIntToShortHelper
    {
        public enum ShortType
        {
            //Oneline,
            //ChangeLine,
            ShortLine,
            LongLine,
        }

        private static BigInteger m_SS = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_RR = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_QQ = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_PP = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_OO = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_NN = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_MM = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_LL = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_KK = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_JJ = new BigInteger("1000000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_II = new BigInteger("1000000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_HH = new BigInteger("1000000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_GG = new BigInteger("1000000000000000000000000000000000000000000000000000000");
        private static BigInteger m_FF = new BigInteger("1000000000000000000000000000000000000000000000000000");
        private static BigInteger m_EE = new BigInteger("1000000000000000000000000000000000000000000000000");
        private static BigInteger m_DD = new BigInteger("1000000000000000000000000000000000000000000000");
        private static BigInteger m_CC = new BigInteger("1000000000000000000000000000000000000000000");
        private static BigInteger m_BB = new BigInteger("1000000000000000000000000000000000000000");
        private static BigInteger m_AA = new BigInteger("1000000000000000000000000000000000000");
        private static BigInteger m_M = new BigInteger("1000000000000000000000000000000000");
        private static BigInteger m_Nonillion = new BigInteger("1000000000000000000000000000000");
        private static BigInteger m_Octillion = new BigInteger("1000000000000000000000000000");
        private static BigInteger m_Septillion = new BigInteger("1000000000000000000000000");
        private static BigInteger m_Sextillion = new BigInteger("1000000000000000000000");
        private static BigInteger m_Quintillion = new BigInteger("1000000000000000000");
        private static BigInteger m_Quadrillion = new BigInteger("1000000000000000");
        private static BigInteger m_Trillion = new BigInteger("1000000000000");
        private static BigInteger m_Billion = new BigInteger("1000000000");
        private static BigInteger m_Million = new BigInteger("1000000");
        private static BigInteger m_Thousand = new BigInteger("1000");

        public static string BigToShortString(BigInteger num, string seq = "",
            ShortType showtype = ShortType.ShortLine)// = "\n"
        {
            if (num > m_SS)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_SS, "ss", seq);
                }
                else
                {
                    return GetString(num, m_SS, "ss", seq);
                }
            }
            else if (num > m_RR)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_RR, "rr", seq);
                }
                else
                {
                    return GetString(num, m_RR, "rr", seq);
                }
            }
            else if (num > m_QQ)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_QQ, "qq", seq);
                }
                else
                {
                    return GetString(num, m_QQ, "qq", seq);
                }
            }
            else if (num > m_PP)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_PP, "pp", seq);
                }
                else
                {
                    return GetString(num, m_PP, "pp", seq);
                }
            }
            else if (num > m_OO)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_OO, "oo", seq);
                }
                else
                {
                    return GetString(num, m_OO, "oo", seq);
                }
            }
            else if (num > m_NN)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_NN, "nn", seq);
                }
                else
                {
                    return GetString(num, m_NN, "nn", seq);
                }
            }
            else if (num > m_MM)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_MM, "mm", seq);
                }
                else
                {
                    return GetString(num, m_MM, "mm", seq);
                }
            }
            else if (num > m_LL)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_LL, "ll", seq);
                }
                else
                {
                    return GetString(num, m_LL, "ll", seq);
                }
            }
            else if (num > m_KK)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_KK, "kk", seq);
                }
                else
                {
                    return GetString(num, m_KK, "kk", seq);
                }
            }
            else if (num > m_JJ)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_JJ, "jj", seq);
                }
                else
                {
                    return GetString(num, m_JJ, "jj", seq);
                }
            }
            else if (num > m_II)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_II, "ii", seq);
                }
                else
                {
                    return GetString(num, m_II, "ii", seq);
                }
            }
            else if (num > m_HH)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_HH, "hh", seq);
                }
                else
                {
                    return GetString(num, m_HH, "hh", seq);
                }
            }
            else if (num > m_GG)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_GG, "gg", seq);
                }
                else
                {
                    return GetString(num, m_GG, "gg", seq);
                }
            }
            else if (num > m_FF)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_FF, "ff", seq);
                }
                else
                {
                    return GetString(num, m_FF, "ff", seq);
                }
            }
            else if (num > m_EE)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_EE, "ee", seq);
                }
                else
                {
                    return GetString(num, m_EE, "ee", seq);
                }
            }
            else if (num > m_DD)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_DD, "dd", seq);
                }
                else
                {
                    return GetString(num, m_DD, "dd", seq);
                }
            }
            else if (num > m_CC)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_CC, "cc", seq);
                }
                else
                {
                    return GetString(num, m_CC, "cc", seq);
                }

            }
            else if (num > m_BB)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_BB, "bb", seq);
                }
                else
                {
                    return GetString(num, m_BB, "bb", seq);
                }

            }
            else if (num > m_AA)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_AA, "aa", seq);
                }
                else
                {
                    return GetString(num, m_AA, "aa", seq);
                }

            }
            else if (num > m_M)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Nonillion, "G", seq);
                }
                else
                {
                    return GetString(num, m_Nonillion, "GG", seq);
                }
            }
            else if (num > m_Nonillion)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Nonillion, "N", seq);
                }
                else
                {
                    return GetString(num, m_Nonillion, "Nonillion", seq);
                }

            }
            else if (num > m_Octillion)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Octillion, "Y", seq);
                }
                else
                {
                    return GetString(num, m_Octillion, "Octillion", seq);
                }
                //return GetString(num, m_Octillion, "Octillion", seq);
            }
            else if (num > m_Septillion)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Septillion, "Z", seq);
                }
                else
                {
                    return GetString(num, m_Septillion, "Septillion", seq);
                }
                //return GetString(num, m_Septillion, "Septillion", seq);
            }
            else if (num > m_Sextillion)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Sextillion, "E", seq);
                }
                else
                {
                    return GetString(num, m_Sextillion, "Sextillion", seq);
                }
                //return GetString(num, m_Sextillion, "Sextillion", seq);
            }
            else if (num > m_Quintillion)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Quintillion, "P", seq);
                }
                else
                {
                    return GetString(num, m_Quintillion, "Quintillion", seq);
                }
                //return GetString(num, m_Quintillion, "Quintillion", seq);
            }
            else if (num > m_Quadrillion)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Quadrillion, "Q", seq);
                }
                else
                {
                    return GetString(num, m_Quadrillion, "Quadrillion", seq);
                }
                //return GetString(num, m_Quadrillion, "Quadrillion", seq);
            }
            else if (num > m_Trillion)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Trillion, "T", "");
                }
                else
                {
                    return GetString(num, m_Trillion, "Trillion", seq);
                }
                //return GetString(num, m_Trillion, "Trillion", seq);
            }
            else if (num > m_Billion)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Billion, "B", "");
                }
                else
                {
                    return GetString(num, m_Billion, "Billion", seq);
                }
                //return GetString(num, m_Billion, "Billion", seq);
            }
            else if (num > m_Million)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Million, "M", "");
                }
                else
                {
                    return GetString(num, m_Million, "Million", seq);
                }
                //return GetString(num, m_Million, "Million", seq);
            }
            else if (num > m_Thousand * 10)
            {
                if (showtype == ShortType.ShortLine)
                {
                    return GetString(num, m_Thousand, "K", "");
                }
                else
                {
                    return GetString(num, m_Thousand, "Thousand", seq);
                }
            }
            else
            {
                return num.ToString();
            }
        }

        private static string GetString(BigInteger num, BigInteger index, string sympol, string seg = "\n")
        {
            return string.Format("{0}.{1}{2}{3}",
                num / index,
                int.Parse(((num % index) / (index / 10)).ToString()),
                seg,
                sympol);
        }

        public static float BigToShortFloat(BigInteger num)
        {
            string result = Regex.Replace(BigToShortString(num), "[A-Z]", "");
            float resultF = 0;
            float.TryParse(result, out resultF);
            return resultF;
        }
    }
}
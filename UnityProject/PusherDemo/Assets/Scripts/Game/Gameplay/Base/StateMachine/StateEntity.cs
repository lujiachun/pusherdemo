﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameWish.Game
{
    public class StateEntity
    {
        protected StateMachineBase m_StateMachine;
        public virtual void Attack()
        {

        }

        public virtual void DoSkill()
        {

        }

        public virtual void Walk()
        {

        }

        public virtual void Idle()
        {

        }
    }
}
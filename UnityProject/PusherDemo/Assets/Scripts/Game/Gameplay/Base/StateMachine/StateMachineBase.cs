﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameWish.Game
{
    public class StateMachineBase
    {
        protected Dictionary<StateEnum, StateBase> m_StateMap = new Dictionary<StateEnum, StateBase>();
        protected StateBase m_CurrentState;
        protected StateEnum m_LastStateEnum;
        protected StateEntity m_Entity;
        protected bool m_IsStart;

        public StateMachineBase(StateEntity entity)
        {
            m_Entity = entity;
        }
        public virtual StateEnum GetCurrentState
        {
            get
            {
                if (m_CurrentState == null)
                    return StateEnum.None;
                return m_CurrentState.StateEnum;
            }
        }

        public void AddState(StateEnum stateEnum, StateBase state)
        {
            m_StateMap.Add(stateEnum, state);
        }

        public void InitState(StateEnum stateEnum)
        {
            if (m_StateMap.ContainsKey(stateEnum))
            {
                m_CurrentState = m_StateMap[stateEnum];
            }
        }

        public void MachineStart()
        {
            if (!m_IsStart && m_CurrentState != null)
            {
                m_IsStart = true;
                m_CurrentState.OnStateIn();
            }
        }

        public void MachineUpdate()
        {
            if (m_IsStart && m_CurrentState != null)
            {
                m_CurrentState.OnStateUpdate();
            }
        }

        public void MachineStop()
        {
            if (m_IsStart && m_CurrentState != null)
            {
                m_IsStart = false;
                m_CurrentState.OnStateOut();
            }
        }

        public virtual void SetState(StateEnum stateEnum)
        {
            if (m_CurrentState != null)
            {
                m_CurrentState.OnStateOut();
                m_LastStateEnum = m_CurrentState.StateEnum;
            }

            if (stateEnum == StateEnum.None)
            {
                m_CurrentState = null;
                return;
            }

            if (m_StateMap.ContainsKey(stateEnum))
            {
                if (!m_IsStart)
                {
                    m_IsStart = true;
                }
                m_CurrentState = m_StateMap[stateEnum];
                m_CurrentState.OnStateIn();
            }
            else
            {
                Debug.LogWarning("not contain " + stateEnum);
            }
        }

        public virtual void BackToLastState()
        {
            SetState(m_LastStateEnum);
        }
    }
}
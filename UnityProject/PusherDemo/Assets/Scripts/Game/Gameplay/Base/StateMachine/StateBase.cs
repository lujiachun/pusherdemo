﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameWish.Game
{
    public class StateBase
    {

        protected StateMachineBase m_StateMachine;
        protected StateEntity m_Entity;
        protected StateEnum m_StateEnum;

        public StateBase(StateMachineBase machine, StateEntity entity)
        {
            m_StateMachine = machine;
            m_Entity = entity;
        }

        public StateEnum StateEnum
        {
            get { return m_StateEnum; }
            set { m_StateEnum = value; }
        }

        public virtual void OnStateIn()
        {

        }

        public virtual void OnStateUpdate()
        {

        }

        public virtual void OnStateOut()
        {

        }

    }
}
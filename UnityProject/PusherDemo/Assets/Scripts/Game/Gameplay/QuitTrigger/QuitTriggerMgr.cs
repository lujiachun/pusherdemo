﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System.Linq;
using System;
using QuickEngine.Extensions;

namespace GameWish.Game
{

    public class QuitTriggerMgr : TMonoSingleton<QuitTriggerMgr>
    {

        public List<QuitTrigger> m_QuitTriggerBaseArray;
        public List<QuitTrigger> m_UnLightQuitTriggerArray;

        private bool m_RollState;
        public void InitMgr() 
        {
            // m_QuitTriggerBaseArray = transform.GetComponentsInChildren<QuitTrigger>().ToList();

            // m_UnLightQuitTriggerArray = new List<QuitTrigger>();

            //  m_QuitTriggerBaseArray.ForEach(v=> 
            // {
            //     v.InitWorldUI();
            //     m_UnLightQuitTriggerArray.Add(v);
            // });
            // EventSystem.S.Register(EventID.OnQuitTriggerEnter, OnQuitTriggerEnter);
            // EventSystem.S.Register(EventID.AllocateDecoration, AllocateDecoration);
           
        }

        // private void AllocateDecoration(int key,params object[] param)
        // {
        //     m_QuitTriggerBaseArray.GetRandomElement().AllocateDecoratipn();
        // }

        // private void OnQuitTriggerEnter(int key, params object[] param)
        // {
        //     if (param == null || param.Length <= 0) 
        //     {
        //         return;
        //     }

        //     if (m_RollState) return;

        //     var trigger = param[0] as QuitTrigger;
        //     trigger.HideDecoration();
        //     if (m_UnLightQuitTriggerArray.Contains(trigger)) 
        //     {
                
        //         trigger.ShowLight();
        //         m_UnLightQuitTriggerArray.Remove(trigger);
        //     }

        //     if (m_UnLightQuitTriggerArray.Count <= 0) 
        //     {
        //         //TODO  将所有以点亮的跑道灭掉
        //         m_QuitTriggerBaseArray.ForEach(v =>
        //         {
        //             v.ShowLight(false);
        //             v.HideWorldUI(false);
        //             m_UnLightQuitTriggerArray.Add(v);
        //         });
        //         //TODO  处理事件发送  用于转动老虎机
        //         EventSystem.S.Send(EventID.OnRollSlotMachine);

        //         m_RollState = true;
        //         //计时3秒钟 进行
        //         StartCoroutine(DoRollTrigger());
        //     }
        // }


        // IEnumerator DoRollTrigger() 
        // {
        //     yield return new WaitForSeconds(0.1f);

        //     for (int i = 0; i < m_QuitTriggerBaseArray.Count; i++)
        //     {
        //         m_QuitTriggerBaseArray[i].ShowLight();
        //         yield return new WaitForSeconds(0.1f);
        //         m_QuitTriggerBaseArray[i].ShowLight(false);
        //     }
        //     yield return new WaitForSeconds(0.1f);
        //     for (int i = m_QuitTriggerBaseArray.Count - 1; i >= 0 ; i--)
        //     {
        //         m_QuitTriggerBaseArray[i].ShowLight();
        //         yield return new WaitForSeconds(0.1f);
        //         m_QuitTriggerBaseArray[i].ShowLight(false);
        //     }

        //     yield return new WaitForSeconds(1f);

        //     m_QuitTriggerBaseArray.ForEach(v =>
        //     {
        //         v.HideWorldUI(true);
       
        //     });

        //     m_RollState = false;
        // }
    }
}
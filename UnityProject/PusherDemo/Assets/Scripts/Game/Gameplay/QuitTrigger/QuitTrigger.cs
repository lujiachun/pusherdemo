﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System.Linq;
using QuickEngine.Extensions;

namespace GameWish.Game {

    public class QuitTrigger : MonoBehaviour
    {
        private int m_Multipe = 1;

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.CompareTag("Tokens"))
            {
                Log.i("Collide");
                GameInfoMgr.data.AddCoins(m_Multipe);
                // 触发老虎机
                EventSystem.S.Send(EventID.OnRollSlotMachine);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.transform.CompareTag("Tokens"))
            {
                GameObjectPoolMgr.S.Recycle(other.gameObject);
            }
        }
    }
}
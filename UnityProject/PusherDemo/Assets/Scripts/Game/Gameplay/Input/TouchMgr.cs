﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using HedgehogTeam.EasyTouch;
using System;

namespace GameWish.Game {


    //Refactor ; 生成逻辑移除单独控制  此类只做输入信息的处理 老虎机奖励分离

    public class TouchMgr : TMonoSingleton<TouchMgr>,IInputObserver
    {
        [SerializeField]
        private GameObject m_TokensPrefab;
        [SerializeField]
        private GameObject m_DolloarPrefab;

        private bool m_HalfGreenProcess = false;
        private int m_CancleID = -1;
        private float m_Time;

        public void InitMgr()
        {
            GameObjectPoolMgr.S.AddPool("TokensPool", m_TokensPrefab, 100, 15);
            GameObjectPoolMgr.S.AddPool("DolloarPool", m_DolloarPrefab, 100, 15);
            EasyTouch.On_TouchDown += On_TouchDown;
            EasyTouch.On_Swipe += On_Swipe;
            EasyTouch.On_LongTap += On_LongTap;
            EasyTouch.On_TouchStart += On_TouchStart;
            EasyTouch.On_TouchUp += On_TouchUp;

            EventSystem.S.Register(EventID.OnProcessHalfGreen, OnProcessHalfGreen);

            EventSystem.S.Register(EventID.OnSlotReward, OnSlotReward);

            InitTokens();
        }

        /// <summary>
        /// 老虎机奖励生成
        /// </summary>
        /// <param name="key"></param>
        /// <param name="param"></param>
        private void OnSlotReward(int key, params object[] param)
        {

            if (param == null || param.Length <= 0) return;

            var item =(SlotRewardType)param[0]; 
 
            switch (item)
            {

                case SlotRewardType.D20:
                    AllocateRandomTokens(20, false);
                    break;
                case SlotRewardType.D50:
                    AllocateRandomTokens(50, false);
                    break;
                case SlotRewardType.G100:
                    AllocateRandomTokens(100);
                    break;
                case SlotRewardType.G50:
                    AllocateRandomTokens(50);
                    break;
                case SlotRewardType.G25:
                    AllocateRandomTokens(25);
                    break;
                case SlotRewardType.Prize777:
                    StartCoroutine(DoProcessPrize777());
                    break;
                default:
                    break;
            }
        }

        private void OnProcessHalfGreen(int key, params object[] param)
        {
            if (param == null || param.Length <= 0) 
            {
                return;
            }

            m_HalfGreenProcess = (bool)param[0];

            if (m_HalfGreenProcess) 
            {
                if (m_CancleID > -1) 
                {
                    Timer.S.Cancel(m_CancleID);                    
                }

                m_CancleID = Timer.S.Post2Scale(v=> 
                {
                    m_HalfGreenProcess = false;
                }, 120);
            }
        }

        public void On_Drag(Gesture gesture, bool isTouchStartFromUI)
        {
            
        }

        public void On_LongTap(Gesture gesture)
        {
            if (gesture.IsOverUIElement()) return;

            m_Time += Time.deltaTime;

            if (m_Time < 0.2f)
            {
                return;
            }
            m_Time = 0;
            
            //if (GameInfoMgr.data.AddToken(-1))
            //{
                /*                Vector3 worldPos = gesture.GetTouchToWorldPoint(-2);
                                float x =  GetPosX(worldPos.x);*/

                RaycastHit raycastHit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out raycastHit, 1000f, 1 << LayerMask.NameToLayer("Board")))
                {
                    AllocateTokens(raycastHit.point.x);
                }


            //}
            //else
            //{
                //UIMgr.S.OpenPanel(UIID.TokensNotEnoughPanel);
            //}

        }

        public void On_Swipe(Gesture gesture)
        {
            if (gesture.IsOverUIElement()) return;

            Debug.LogError("1111" + gesture.deltaPosition);

            m_Time += Time.deltaTime;

            if (m_Time < 0.3f)
            {
                return;
            }
            m_Time = 0;
            // if (GameInfoMgr.data.AddToken(-1))
            // {
                RaycastHit raycastHit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out raycastHit, 1000f, 1 << LayerMask.NameToLayer("Board")))
                {
                    AllocateTokens(raycastHit.point.x);
                }
            // }
            // else
            // {
            //     UIMgr.S.OpenPanel(UIID.TokensNotEnoughPanel);
            // }
        }

        public void On_TouchDown(Gesture gesture)
        {
            
        }

        /// <summary>
        /// 触摸事件
        /// </summary>
        /// <param name="gesture"></param>
        public void On_TouchStart(Gesture gesture)
        {
            if (gesture.IsOverUIElement()) return;
            AudioMgr.S.PlaySoundShoot("click");
            RaycastHit raycastHit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out raycastHit, 1000f))
            {
                AllocateTokens(raycastHit.point.x);
            }
        }

        public void On_TouchUp(Gesture gesture)
        {
            
        }

        

        /// <summary>
        /// 触摸生成代币
        /// </summary>
        /// <param name="posx"></param>
        /// <param name="random"></param>
        public void AllocateTokens(float posx, bool random = false) 
        {
            if (random) posx = RandomHelper.Range(-12, 12);
            posx = Mathf.Clamp(posx, -12, 12);
            GameObject obj = GameObjectPoolMgr.S.Allocate("TokensPool");
            obj.transform.SetParent(this.transform, false);
            obj.transform.position = new Vector3(posx, 30, 30);
            obj.transform.localEulerAngles = new Vector3(0, 0, 0);
            AudioMgr.S.PlaySoundShoot("coindrop");
        }

        /// <summary>
        /// 开局生成部分代币
        /// </summary>
        public void InitTokens()
        {
            AllocateRandomTokens(20);
        }

        /// <summary>
        /// 开始时的代币生成
        /// </summary>
        public void AllocateRandomTokens(int count)
        {
            for (int i = 0; i < count; i++)
            {
                int posx = RandomHelper.Range(-15, 15);
                posx = Mathf.Clamp(posx, -15, 15);
                int posy = RandomHelper.Range(3, 9);
                posy = Mathf.Clamp(posy, 3, 9);
                int posz = RandomHelper.Range (-20, 10);
                posz = Mathf.Clamp(posz, -20, 10);
                GameObject obj = GameObjectPoolMgr.S.Allocate("TokensPool");
                obj.transform.SetParent(this.transform, false);
                obj.transform.position = new Vector3(posx, posy, posz);
            }
        }

        private void AllocateRandomTokens(int count, bool type = true) 
        {
            if (type)
            {
                for (int i = 0; i < count; i++)
                {
                    AllocateTokens(0, true); 
                }
                AudioMgr.S.PlaySoundShoot("slot-coinout");
            }
            else 
            {
                for (int i = 0; i < count; i++)
                {
                    AllocateTokens(0, true);
                }
                AudioMgr.S.PlaySoundShoot("slot-coinout");
            }
        }


        public void AllocateTokens()
        {
            float posx = RandomHelper.Range(-10, 10);
            posx = Mathf.Clamp(posx, -10, 10);

            GameObject obj;
            {
                obj = GameObjectPoolMgr.S.Allocate("TokensPool");
            }

            obj.transform.SetParent(this.transform);

            obj.transform.position = new Vector3(posx, 20, 0);
            obj.transform.localEulerAngles = new Vector3(90, 0, 0);
            AudioMgr.S.PlaySoundShoot("coindrop");
        }

        public void AllocateDolloar()
        {
            float posx = RandomHelper.Range(-21, 21);
            posx = Mathf.Clamp(posx, -21, 21);

            GameObject obj;
      
           obj = GameObjectPoolMgr.S.Allocate("DolloarPool");
            

            obj.transform.SetParent(this.transform);

            obj.transform.position = new Vector3(posx, 32, 0);
            AudioMgr.S.PlaySoundShoot("coindrop");
        }



        IEnumerator DoProcessPrize777() 
        {
            yield return null;

            bool isGreen = GameInfoMgr.data.GetDollar() < 50;

            string poolname = isGreen ? "DolloarPool" : "TokensPool";

            int count = 9;

            while (count > 0) 
            {
                for (int i = 0; i < 9; i++)
                {
                    int dex = 24;

                    if (i == 0 || i == 8) 
                    {
                        dex = 22;
                    }

                    AllocatePrize(poolname, i * 6f - dex);
                    
                }
                AudioMgr.S.PlaySoundShoot("slot-coinout");
                yield return new WaitForSeconds(0.2f);
                count--;
            }

            for (int i = 0; i < 7; i++)
            {
                AllocatePrize(poolname, i * 6f - 18);
            }
            AudioMgr.S.PlaySoundShoot("slot-coinout");
            yield return new WaitForSeconds(0.2f);

            for (int i = 0; i < 3; i++)
            {
                AllocatePrize(poolname, i * 6f - 6);
            }
            AudioMgr.S.PlaySoundShoot("slot-coinout");
            yield return new WaitForSeconds(0.35f);

            SceneItemMgr.S.SetAllobsState(true);

        }

        public void AllocatePrize(string poolname, float posx)
        {
            GameObject obj;

            obj = GameObjectPoolMgr.S.Allocate(poolname);


            obj.transform.SetParent(this.transform);

            obj.transform.position = new Vector3(posx, 30, 0);
            AudioMgr.S.PlaySoundShoot("coindrop");

        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Qarth;

public class CylinderCreate : Editor
{


    [MenuItem("GameObject/Create Cylinder Grid")]
    public static void CreateCylinderGrid() 
    {

        EditorWindow.GetWindow(typeof(CylinderCreateWindow),true);

    }



}

public class CylinderCreateWindow : EditorWindow 
{
    string titleContentLabel = "";
    int rowCount = 10;
    int colCount = 10;
    float startPosx = 0;
    float spaceX = 1;
    float spaceY = 1;
    GameObject cylider;
    CylinderCreateWindow() 
    {
        titleContent = new GUIContent("生成器");
    }


    private void OnGUI()
    {
        GUILayout.BeginVertical();

        GUILayout.Space(10);
        GUI.skin.label.fontSize = 12;
        rowCount = int.Parse( EditorGUILayout.TextField("Row Count", rowCount.ToString()));
        colCount = int.Parse(EditorGUILayout.TextField("Column Count", colCount.ToString()));
        startPosx = float.Parse(EditorGUILayout.TextField("startPosx", startPosx.ToString()));
        spaceX = float.Parse(EditorGUILayout.TextField("spaceX Count", spaceX.ToString()));
        spaceY = float.Parse(EditorGUILayout.TextField("spaceY Count", spaceY.ToString()));
        cylider = EditorGUILayout.ObjectField("选取预设", cylider, typeof(GameObject)) as GameObject;
        if (GUILayout.Button("Create")) 
        {
            Create();
        }
    }

    void Create()
    {
        GameObject g1o =  GameObject.Find("CylinderCreate");
        if (g1o != null) 
        {
            DestroyImmediate(g1o);
        }
        GameObject go = new GameObject();
        go.name = "CylinderCreate";

        for (int j = 0; j < rowCount ; j++)
        {

            int colc = colCount - j % 2 ;

            float startpos = colc % 2 == 0 ? startPosx + spaceX / 2 : startPosx;
            for (int i = 0; i < colc; i++)
            {
                GameObject cyl1 = GameObject.Instantiate(cylider);

                cyl1.name = "c"+j + "-" + i;

                cyl1.transform.SetParent(go.transform);
                //cyl1.transform.localPosition = new Vector3(startpos + spaceX* i, spaceY*(j-1));

                cyl1.transform.localPosition = new Vector3(startpos + spaceX * i, 0, spaceY * (j - 1));
                //cyl1.transform.Rotate(new Vector3(90, 0 ,0));
                //cyl1.transform.localScale = Vector3.one * 0.2f;
/*                Material  mat = new Material(Shader.Find("Standard"));
                mat.color = Color.green;
                cyl1.GetComponent<Renderer>().material = mat; */
            }
        }


        


    }
}

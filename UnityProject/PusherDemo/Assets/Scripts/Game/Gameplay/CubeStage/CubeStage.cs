﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace GameWish.Game
{

    public class CubeStage : MonoBehaviour
    {
        private Rigidbody m_Rigi;
        private void Awake()
        {
            Sequence sequence = DOTween.Sequence();
            sequence.Append(transform.DOLocalMoveZ(-36, 1f)).OnComplete(() =>
            {
                transform.DOLocalMoveZ(-52, 2f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
            });
        }
    }
}
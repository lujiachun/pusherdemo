﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Qarth;
using System.Linq;
using QuickEngine.Extensions;

namespace GameWish.Game
{
    public class CubeStage2 : MonoBehaviour
    {
        private void Awake()
        {
            // 左右平移
            // transform.DOBlendableLocalMoveBy(new Vector3(-42 , 0, 0), 8f).OnComplete(() =>
            // {
            //     transform.DOBlendableLocalMoveBy(new Vector3(42 , 0, 0), 8f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
            // });
            // // 自身旋转
            // transform.DOBlendableLocalRotateBy(new Vector3(0, 0, -90), 5f).OnComplete(() =>
            // {
            //     transform.DOBlendableLocalRotateBy(new Vector3(0, 0, 0), 5f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
            // });
            

            Sequence sequence = DOTween.Sequence();
            sequence.Append(transform.DOLocalRotate(new Vector3(0, 0, 90), 1f).SetEase(Ease.Linear));
            sequence.Append(transform.DOLocalRotate(new Vector3(0, 0, 0), 1f).SetEase(Ease.Linear));
            sequence.Append(transform.DOLocalMoveX(-21, 4f).SetEase(Ease.Linear));
            sequence.Append(transform.DOLocalRotate(new Vector3(0, 0, -90), 1f).SetEase(Ease.Linear));
            sequence.Append(transform.DOLocalRotate(new Vector3(0, 0, 0), 1f).SetEase(Ease.Linear));
            sequence.Append(transform.DOLocalMoveX(21, 4f).SetEase(Ease.Linear));
            sequence.SetLoops(-1, LoopType.Restart);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Qarth;
using QuickEngine.Extensions;

namespace GameWish.Game
{
    public static class GameExtensions
    {
        public static void ShareGame()
        {
            DataAnalysisMgr.S.CustomEvent(Define.EVT_SHARE);
            SocialMgr.S.ShareTextWithURL("Let's play!",
                "Such a Funny Game!",
                "https://play.google.com/");
        }

        public static string GetMaskImg(string name)
        {
            return string.Format("{0}_h", name);
        }

        public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
        {
            return Mathf.Atan2(
                       Vector3.Dot(n, Vector3.Cross(v1, v2)),
                       Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
        }


        public static String[] dicimalToFraction(float inNum)
        {
            string result = default(string);
            string[] array = inNum.ToString().Split('.');
            if (array.Length < 2)
            {
                return new string[] { array[0], "1" };
            }
            int len = array[1].Length;
            int num = Convert.ToInt32(Math.Pow(10, len));
            int value = Convert.ToInt32(inNum * num);
            int a = value;
            int b = num;
            while (a != b)
            {
                if (a > b)
                    a = a - b;
                else
                    b = b - a;
            }

            value = value / a;
            num = num / a;
            return new string[]
            {
                value.ToString(), num.ToString()
            };
        }



        public static int GetDaysPass(string data)
        {
            System.DateTime pauseT = System.Convert.ToDateTime(data);
            System.DateTime resumeT = System.DateTime.Today;
            System.TimeSpan ts1 = new System.TimeSpan(pauseT.Ticks);
            System.TimeSpan ts2 = new System.TimeSpan(resumeT.Ticks);
            System.TimeSpan tsSub = ts1.Subtract(ts2).Duration();
            return tsSub.Days;
        }
        public static int GetSecondsPass(string oldData, string newData)
        {
            System.DateTime oldT = System.Convert.ToDateTime(oldData);
            System.DateTime newT = System.Convert.ToDateTime(newData);
            System.TimeSpan ts1 = new System.TimeSpan(oldT.Ticks);
            System.TimeSpan ts2 = new System.TimeSpan(newT.Ticks);
            return (int)(ts1 - ts2).TotalSeconds;
        }
        public static int RandomByWeight(List<int> _weights, int maxIndex = 0)
        {
            List<int> weights = new List<int>();
            for (int i = 0; i < _weights.Count; i++)
            {
                weights.Add(_weights[i]);
            }
            if (weights == null || weights.Count < 2)
            {
                return maxIndex;
            }
            for (int i = 1; i < weights.Count; i++)
            {
                weights[i] += weights[i - 1];
            }
            int sum = weights[weights.Count - 1];
            int number_rand = RandomHelper.Range(0, sum);

            if (number_rand < weights[0])
                return 0;

            for (int i = 1; i < weights.Count; i++)
            {
                if (weights[i - 1] <= number_rand && number_rand < weights[i])
                    return i;
            }
            return -1;
        }

        public static int RandomByWeightIndex(List<int> _weights, int maxIndex = 0)
        {
            List<int> weights = new List<int>();
            for (int i = 0; i < _weights.Count; i++)
            {
                for (int j = 0; j < _weights[i]; j++)
                {
                    weights.Add(i);
                }
            }
            /*            if (weights == null || weights.Count < 2)
                        {
                            return maxIndex;
                        }
                        for (int i = 1; i < weights.Count; i++)
                        {
                            weights[i] += weights[i - 1];
                        }
                        int sum = weights[weights.Count - 1];
                        int number_rand = RandomHelper.Range(0, sum);

                        if (number_rand < weights[0])
                            return 0;

                        for (int i = 1; i < weights.Count; i++)
                        {
                            if (weights[i - 1] <= number_rand && number_rand < weights[i])
                                return i;
                        }*/
            return weights[RandomHelper.Range(0, weights.Count)];
        }


    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class ParticleCtrller : MonoBehaviour
    {
        public float m_Time = 0;
        public float m_maxTime = 0;

        ParticleSystem[] particle_systems;
        ParticleSystemRenderer[] particle_renders;
        List<ParticleSystem> listLoopPs = new List<ParticleSystem>();
        Animation[] particle_animations;

        SpriteRenderer[] particle_sprites;

        Dictionary<ParticleSystem, float> systemTimeList = new Dictionary<ParticleSystem, float>();
        Dictionary<Renderer, int> originSorts = new Dictionary<Renderer, int>();

        public System.Action overCallback;
        bool m_ForeverExistTag;

        public void Awake()
        {
#if UNITY_EDITOR
            gameObject.AddComponent<CustomShaderFinder>();
#endif

            particle_systems = GetComponentsInChildren<ParticleSystem>();
            particle_renders = GetComponentsInChildren<ParticleSystemRenderer>();
            particle_animations = GetComponentsInChildren<Animation>();
            particle_sprites = GetComponentsInChildren<SpriteRenderer>();

            if (particle_systems != null)
            {
                for (int i = 0; i < particle_renders.Length; i++)
                {
                    originSorts.Add(particle_renders[i], particle_renders[i].sortingOrder);
                }
            }
            if (particle_sprites != null)
            {
                for (int i = 0; i < particle_sprites.Length; i++)
                {
                    originSorts.Add(particle_sprites[i], particle_sprites[i].sortingOrder);
                }
            }
        }

        public void CalcEffMaxTime()
        {
            m_maxTime = 0;
            if (particle_systems != null && particle_animations.Length > 0)
            {
                float length = 0;
                foreach (Animation anim in particle_animations)
                {
                    if (anim.clip != null)
                    {
                        length = anim.clip.length > length ? anim.clip.length : length;
                    }
                }

                m_maxTime = length;
            }

            if (particle_animations != null && particle_systems.Length > 0)
            {
                float durLen = 0;
                float lifeLen = 0;
                float delayLen = 0;
                foreach (ParticleSystem system in particle_systems)
                {
                    delayLen = system.startDelay > delayLen ? system.startDelay : delayLen;
                    lifeLen = system.startLifetime > lifeLen ? system.startLifetime : lifeLen;
                    durLen = system.duration > durLen ? system.duration : durLen;
                    if (system.loop)
                        listLoopPs.Add(system);
                }

                var maxBase = Mathf.Max(lifeLen, durLen);
                float newTime = maxBase + delayLen;
                m_maxTime = newTime > m_maxTime ? newTime : m_maxTime;
            }
        }

        public void OnEnable()
        {
            m_Time = 0;
            CalcEffMaxTime();
            foreach (ParticleSystem system in particle_systems)
            {
                if (listLoopPs.Contains(system))
                    system.loop = true;
                system.enableEmission = true;
            }

            if (m_ForeverExistTag)
                SetMaxTimeUseless();
        }

        public void Update()
        {

            m_Time += Time.deltaTime;

            if (m_maxTime >= 0 && m_Time >= m_maxTime)
            {
                DestroyEffectGradually();
            }

        }


        void OnDestroy()
        {
            particle_renders = null;
            particle_systems = null;
            particle_animations = null;
            particle_sprites = null;
            systemTimeList.Clear();
            listLoopPs.Clear();
            originSorts.Clear();
        }

        //! 销毁特效
        public void DestroyEffect(bool recycle = true)
        {
            m_ForeverExistTag = false;
            SetMaxTimeUseless();

            foreach (ParticleSystem system in particle_systems)
            {
                system.Stop();
            }

            if (recycle)
                Lean.Pool.LeanPool.Despawn(gameObject);
            else
                GameObject.Destroy(gameObject);
        }

        //! 使MaxTime控制删除失效
        public void SetMaxTimeUseless(bool forever = false)
        {
            m_ForeverExistTag = forever;
            m_maxTime = -1;
        }

        public void ResetMaxTimeUseful(float newTime = -1)
        {
            if (newTime == -1)
                m_maxTime = m_Time;
            else
                m_maxTime = newTime;
        }


        public void DestroyEffectGradually()
        {
            m_ForeverExistTag = false;
            //Debug.Log(gameObject.name);
            if (particle_systems.Length > 0)
            {
                bool stop = true;
                foreach (ParticleSystem system in particle_systems)
                {
                    if (system.main.loop == true)
                    {
                        system.loop = false;
                    }
                    system.enableEmission = false;

                    if (system.isPlaying)
                    {
                        stop = false;
                    }
                }

                if (stop || m_Time > m_maxTime + 5)//一般duration不会超过5
                {
                    if (overCallback != null)
                    {
                        overCallback.Invoke();
                        overCallback = null;
                    }
                    Lean.Pool.LeanPool.Despawn(gameObject);
                }
            }
            else
            {
                if (overCallback != null)
                {
                    overCallback.Invoke();
                    overCallback = null;
                }
                Lean.Pool.LeanPool.Despawn(gameObject);
            }
        }


        public void SetAllLayers(int layer)
        {
            if (particle_systems != null)
            {
                for (int i = 0; i < particle_systems.Length; i++)
                {
                    particle_renders[i].sortingOrder = originSorts[particle_renders[i]] + layer;
                }
            }
            if (particle_sprites != null)
            {
                for (int i = 0; i < particle_sprites.Length; i++)
                {
                    particle_sprites[i].sortingOrder = originSorts[particle_sprites[i]] + layer;
                }
            }
        }
    }

}
﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class GameTempDataMgr : TMonoSingleton<GameTempDataMgr>
    {
        private bool isFirstPassThisLv = false;
        public bool IsFirstPassThisLv { get => isFirstPassThisLv; set => isFirstPassThisLv = value; }
        private bool isGameLose = false;
        public bool IsGameLose { get => isGameLose; set => isGameLose = value; }
    }
}

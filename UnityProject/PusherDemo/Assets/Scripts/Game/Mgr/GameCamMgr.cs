﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using Qarth;
//using EZCameraShake;
//using DG.Tweening;

//namespace GameWish.Game
//{
//    public class GameCamMgr : TMonoSingleton<GameCamMgr>
//    {
//        [SerializeField]
//        private Camera m_Cam;
//        [SerializeField]
//        private Camera m_EffCam;

//        [SerializeField]
//        private Vector3 m_Offset = new Vector3(0, 0.5f, -2f);
//        [SerializeField]
//        private Transform m_TrsCamTarget;

//        [SerializeField]
//        private float m_SmoothSpeed = 100f;

//        [SerializeField]
//        private Vector3 m_ShakeCamParams = new Vector3(2f, 15f, 0.1f);
//        //[SerializeField]
//        //private SpriteRenderer sprite;

//        private bool m_OpenFollow;



//        private CameraShaker m_Shaker;
//        private CameraShakeInstance m_ShakeInstance;

//        private Vector3 m_OriginPos;
//        private Vector3 m_OriginRotate;

//        private float m_BaseSize;
//        private float m_ScreenHeight;
//        private float m_ScreenWidth;
//        private float m_HeightRatio = 1;
//        private float m_WidthRatio = 1;
//        private Vector3 m_TopDistance = new Vector3(0, 0.687f, 0);
//        private Vector3 m_BottomDistance = new Vector3(0, 1.38f, 0);

//        public float ScreenHeight
//        {
//            get
//            {
//                return m_ScreenHeight;
//            }
//        }
//        public float ScreenWidth
//        {
//            get
//            {
//                return m_ScreenWidth;
//            }
//        }

//        public Camera gameplayCamera
//        {
//            get { return m_Cam; }
//        }

//        public Vector3 camPosition
//        {
//            set { transform.position = value; }
//            get { return transform.position; }
//        }

//        public override void OnSingletonInit()
//        {
//            m_OriginPos = transform.position;
//            m_OriginRotate = transform.eulerAngles;

//            m_Shaker = GetComponentInChildren<CameraShaker>();
//            m_BaseSize = m_Cam.orthographicSize;

//            //m_Cam.fieldOfView = (Screen.height * 1f / Screen.width) / (1334f / 750 / 60);
//            //m_ScreenHeight = m_BaseSize * 2;
//            //m_ScreenWidth = m_ScreenHeight * m_Cam.aspect;

//            //float scaleY = m_ScreenHeight / sprite.sprite.bounds.size.y;
//            //float scaleX = m_ScreenWidth / sprite.sprite.bounds.size.x;
//            //sprite.transform.localScale = new Vector3(scaleX, scaleY, 1);
//            //Debug.Log(m_BaseSize);

//            Log.i("InitCameraMgr");
//        }

//        public void AdjustCamSize()
//        {
//            var hwRate = Screen.height * 1.0f / Screen.width;
//            if (hwRate > 1.778f)
//            {
//                m_Cam.orthographicSize = m_BaseSize * (hwRate / 1.778f);
//                m_EffCam.orthographicSize = m_BaseSize * (hwRate / 1.778f);
//            }
//        }

//        public void ResetCam()
//        {
//            transform.position = m_OriginPos;
//            transform.eulerAngles = m_OriginRotate;
//        }

//        public void SetFollowState(bool state)
//        {
//            m_OpenFollow = state;
//        }

//        private void LateUpdate()
//        {
//            if (m_TrsCamTarget != null && m_OpenFollow)
//            {
//                transform.position = Vector3.Lerp(transform.position, new Vector3(m_TrsCamTarget.position.x, 0, m_TrsCamTarget.position.z) + m_Offset, m_SmoothSpeed * Time.deltaTime);
//            }
//        }

//        public void SetTargeter(Transform target)
//        {
//            m_TrsCamTarget = target;
//        }

//        protected void CleanCam()
//        {
//            m_ShakeInstance.DeleteOnInactive = true;
//            m_ShakeInstance = null;
//        }

//        #region CamFuncs
//        public void AddCamTarget(Transform trsTarget)
//        {
//            m_TrsCamTarget = trsTarget;
//        }

//        public void StartShakeCam(float magn = 0.1f, float rough = 5f, float fadeInTime = 0.2f)
//        {
//            if (m_ShakeInstance == null)
//            {
//                m_ShakeInstance = CameraShaker.Instance.StartShake(magn, rough, fadeInTime);
//                m_ShakeInstance.DeleteOnInactive = false;
//            }
//            else
//            {
//                m_ShakeInstance.ScaleMagnitude = magn;
//                m_ShakeInstance.ScaleRoughness = rough;
//                m_ShakeInstance.StartFadeIn(fadeInTime);
//            }
//        }

//        public void StopShakeCam(float fadeOutTime = 0.2f)
//        {
//            if (m_ShakeInstance != null)
//                m_ShakeInstance.StartFadeOut(fadeOutTime);
//        }

//        public void SmoothCam(float sizeRate, float duration = 0.3f)
//        {
//            if (m_Cam != null)
//            {
//                m_Cam.DOOrthoSize(m_BaseSize * sizeRate, duration);
//            }
//        }

//        public void ShakeExtraOnce(float magn = 0.1f, float rough = 5f, float fadeTime = 0.2f)
//        {
//            CameraShakeInstance c = CameraShaker.Instance.ShakeOnce(magn, rough, fadeTime, fadeTime);
//            c.PositionInfluence = m_Shaker.DefaultPosInfluence;
//            c.RotationInfluence = m_Shaker.DefaultRotInfluence;
//        }
//        #endregion
//    }
//}

﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Reflection;
using GameWish.Game;

namespace Qarth
{
    public partial class UIMgr
    {
        // /// <summary>
        // /// 寻找panel下面的panel并关闭下面的panel
        // /// </summary>
        // /// <param name="ui"></param>
        // public void FindBotUIFromThisPanel(UIID ui)
        // {
        //     for (int i = m_ActivePanelInfoList.Count - 1; i >= 0; i--)
        //     {
        //         if (UIMgr.S.FindPanel(ui) != null && m_ActivePanelInfoList[i].uiID != (int)UIID.TopPanel && m_ActivePanelInfoList[i].uiID != (int)UIID.WorldUIPanel && m_ActivePanelInfoList[i].uiID != (int)UIID.PigeonVideoPanel && m_ActivePanelInfoList[i].uiID != (int)UIID.UIClipPanel && m_ActivePanelInfoList[i].uiID != (int)UIID.MainPanel)
        //         {
        //             if (m_ActivePanelInfoList[i].abstractPanel.maxSortingOrder < UIMgr.S.FindPanel(ui).maxSortingOrder)
        //             {
        //                 m_ActivePanelInfoList[i].abstractPanel.CloseSelfPanel();
        //             }
        //         }
        //     }
        // }

        /// <summary>
        /// 判断界面是否有其他UI遮挡
        /// </summary>
        /// <param name="ui"></param>
        /// <returns></returns>
        public bool HasTopPanelInPanel(UIID ui)
        {
            for (int i = m_ActivePanelInfoList.Count - 1; i >= 0; i--)
            {
                if (UIMgr.S.FindPanel(ui) != null && m_ActivePanelInfoList[i].uiID != (int)UIID.MainPanel && m_ActivePanelInfoList[i].uiID != (int)UIID.WorldUIPanel
                    && m_ActivePanelInfoList[i].uiID != (int)EngineUI.FloatMessagePanel && m_ActivePanelInfoList[i].uiID != (int)EngineUI.MaskPanel)
                {
                    if (m_ActivePanelInfoList[i].abstractPanel.maxSortingOrder > UIMgr.S.FindPanel(ui).maxSortingOrder && m_ActivePanelInfoList[i].abstractPanel.hasOpen)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool HasPanelActiveInScene(UIID ui)
        {
            for (int i = m_ActivePanelInfoList.Count - 1; i >= 0; i--)
            {
                if (UIMgr.S.FindPanel(ui) != null && m_ActivePanelInfoList[i].uiID == (int)ui)
                {
                    return true;
                }
            }
            return false;
        }

        // public bool CheckCanShowUpgradePanel()
        // {
        //     for (int i = m_ActivePanelInfoList.Count - 1; i >= 0; i--)
        //     {
        //         if (m_ActivePanelInfoList[i].uiID == (int)UIID.BuffPanel || m_ActivePanelInfoList[i].uiID == (int)UIID.ShopPanel
        //             || m_ActivePanelInfoList[i].uiID == (int)UIID.TaskPanel || m_ActivePanelInfoList[i].uiID == (int)UIID.TipPanel || m_ActivePanelInfoList[i].uiID == (int)UIID.SettingPanel || m_ActivePanelInfoList[i].uiID == (int)UIID.OpenBagPanel || m_ActivePanelInfoList[i].uiID == (int)UIID.OfflinePanel)
        //         {
        //             return false;
        //         }
        //     }
        //     return true;
        // }

    }
}
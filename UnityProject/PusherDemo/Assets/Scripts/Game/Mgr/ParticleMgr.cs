﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class ParticleMgr : TMonoSingleton<ParticleMgr>
    {

        /// <summary>
        /// 深度遍历设置所有 Render 的 SortingLayer
        /// </summary>
        /// <param name="target"></param>
        /// <param name="order"></param>
        /// <param name="isRecursion"></param>
        public void SetRenderSortingLayer(GameObject target, int order, bool isRecursion)
        {
            if (target != null)
            {
                SetSpriteRenderSortingLayer(target, order);
                SetParticleSystemSortingLayer(target, order);
                SetMeshRenderSortingLayer(target, order);

                int childCount = target.transform.childCount;
                int idx = 0;
                Transform childTrans = null;

                if (isRecursion)
                {
                    for (idx = 0; idx < childCount; ++idx)
                    {
                        childTrans = target.transform.GetChild(idx);
                        SetRenderSortingLayer(childTrans.gameObject, order, isRecursion);
                    }
                }
            }
        }

        public void SetSpriteRenderSortingLayer(UnityEngine.GameObject target, int order)
        {
            if (target != null)
            {
                SpriteRenderer render = null;
                render = target.GetComponent<SpriteRenderer>();

                SetSpriteRenderSortingLayerBySpriteRenderer(render, order);
            }
        }

        // 可以在编辑器中设置
        public void SetSpriteRenderSortingLayerBySpriteRenderer(SpriteRenderer render, int order)
        {
            if (render != null && render.sortingOrder != order)
            {
                render.sortingOrder = order;
            }
        }

        public void SetParticleSystemSortingLayer(UnityEngine.GameObject target, int order)
        {
            if (target != null)
            {
                ParticleSystem particleSystem = null;
                particleSystem = target.GetComponent<ParticleSystem>();

                SetParticleSystemSortingLayer(particleSystem, order);
            }
        }

        // 不能在编辑器中设置
        public void SetParticleSystemSortingLayer(ParticleSystem particleSystem, int order)
        {
            if (particleSystem != null)
            {
                Renderer render = particleSystem.GetComponent<Renderer>();
                if (render != null && render.sortingOrder != order)
                {
                    render.sortingOrder = order;
                }
            }
        }

        public void SetMeshRenderSortingLayer(UnityEngine.GameObject target, int order)
        {
            if (target != null)
            {
                UnityEngine.MeshRenderer meshRenderer = null;
                meshRenderer = target.GetComponent<UnityEngine.MeshRenderer>();

                SetMeshRenderSortingLayer(meshRenderer, order);
            }
        }

        // 不能在编辑器中设置
        public void SetMeshRenderSortingLayer(UnityEngine.MeshRenderer meshRenderer, int order)
        {
            if (meshRenderer != null && meshRenderer.sortingOrder != order)
            {
                meshRenderer.sortingOrder = order;
            }
        }

    }
}

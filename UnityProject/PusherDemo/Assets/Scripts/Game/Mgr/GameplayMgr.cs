﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System;
using HedgehogTeam.EasyTouch;
using Spine.Unity;
using Spine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using EventSystem = Qarth.EventSystem;

namespace GameWish.Game
{
    public class GameplayMgr : TMonoSingleton<GameplayMgr>
    {
        private GameInfoMgr m_GameInfoMgr;
        private ResLoader m_Loader;
        public ResLoader GameResLoader
        {
            get { return m_Loader; }
        }

        private SpritesHandler m_SprHandler;
        public SpritesHandler GameSprHandler
        {
            get { return m_SprHandler; }
        }

        private int m_GameplayTimer;
        private bool m_Inited = false;

        private bool m_IsPlaying = false;

        public bool isPlaying
        {
            get { return m_IsPlaying; }
            set { m_IsPlaying = value; }
        }

        private float m_BuzzLocker;

        public void InitGameplay()
        {
            Input.multiTouchEnabled = false;
         
            TouchMgr.S.InitMgr();
            BGMgr.S.InitBG();
            SceneItemMgr.S.Init();
            UIMgr.S.OpenPanel(UIID.WorldUIPanel);
            m_GameInfoMgr = new GameInfoMgr();
            // if (string.IsNullOrEmpty(CreditLocalInfoMgr.data.userId) && !CustomExtensions.GetSDKChannel().Equals("taptap"))//PlayerPrefs.GetString(CreditDefine.USERID_TEMP_SAVE_KEY)))
            // {
            //     LoginPanel.S.Init();
            //     LoginPanel.OnLoginCallBack += LoginCallBack;
            //     UIMgr.S.ClosePanelAsUIID(UIID.LogoPanel);
            //     //UIMgr.S.OpenTopPanel(CreditUI.CreditLoginPanel, null);
            // }
            // else
            // {
            //     UIMgr.S.ClosePanelAsUIID(UIID.LogoPanel);


            //     CreditMgr.S.Init();

            //     
            //     LoadRes();
            //     //关闭时机--没有登陆界面时初始化后关闭
            //     

            // }
            StartGameplay();
        }

        public void LoginCallBack(string wechatCode)
        {
            Timer.S.Post2Scale(v =>
            {
                //CreditMgr.S.Init(wechatCode);
                
                LoadRes();
                //UIMgr.S.ClosePanelAsUIID(CreditUI.CreditLoginPanel);
                StartGameplay();

            }, 0.8f, 1);

        }

        void LoadRes()
        {
            m_Loader = ResLoader.Allocate(this.GetType().Name);
            m_SprHandler = new SpritesHandler();
            InitGamePool();
            InitAudio();
        }

        void StartGameplay()
        {
            UIMgr.S.ClosePanelAsUIID(UIID.LogoPanel);
            
            LoadRes();

            EventSystem.S.Register(EngineEventID.OnApplicationPauseChange, OnGamePauseChange);
            EventSystem.S.Register(EngineEventID.OnApplicationFocusChange, OnGameFocusChange);
            EventSystem.S.Register(EngineEventID.OnApplicationQuit, OnGameQuit);

            // GetPropertyMgr.S.Init();
            CheckTimeReward();
            
            UIMgr.S.OpenPanel(UIID.SlotMachinePanel);
            UIMgr.S.OpenPanel(UIID.MainPanel);
        }

        void InitAudio()
        {
            PlayBGM("main_bgm");
            SoundButton.defaultClickSound = TDConstTable.QueryString(ConstType.SOUND_DEFAULT_BUTTON);
        }

        public void PlayBGM(string name, float volume = 1f)
        {
            AudioMgr.S.PlayBg(name, true);
            AudioMgr.S.SetVolume(AudioMgr.S.GetBGID(), volume);
        }

        protected void InitGamePool()
        {

        }

        public void StartTimeRecord()
        {
            OnGameTimeRecord(0);
            m_GameplayTimer = Timer.S.Post2Really(OnGameTimeRecord, 60, -1);
        }

        private void OnGamePauseChange(int key, params object[] args)
        {
            bool pause = (bool)args[0];
            if (!pause)
            {
                Log.i("Game Unpause.");
                CheckTimeReward(true);
            }

        }
        private void OnGameFocusChange(int key, params object[] args)
        {
            bool focusState = (bool)args[0];
            if (focusState)
            {
                return;
            }
/*            PlayerInfoMgr.Save();*/
        }

        private void OnGameQuit(int key, params object[] args)
        {
/*            PlayerInfoMgr.Save();*/
        }


        #region OFFLINE_REWARD

        //每隔一段时间请求一下服务器时间存档作为最后游玩时间(ms时间戳)
        private void OnGameTimeRecord(int count)
        {
/*            PlayerInfoMgr.data.SetLastPlayTime(CustomExtensions.GetTimeStamp());
            PlayerInfoMgr.data.ResetDailyParams();
            PlayerInfoMgr.Save();*/
        }

        void CheckTimeReward(bool isPauseCheck = false)
        {
            /*string lastTimeStr = PlayerInfoMgr.data.lastPlayTimeString;
            if (!string.IsNullOrEmpty(lastTimeStr) && lastTimeStr != "0")
            {
                DateTime dtStart = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                var timeStr = CustomExtensions.GetTimeStamp();
                //有游玩记录但是联网失败就不刷新计时也不给奖
                if (!string.IsNullOrEmpty(timeStr))
                {
                    //GameData.LastPlayTimeString = timeStr;
                    long longTimeLast = long.Parse(timeStr);
                    long.TryParse(lastTimeStr, out longTimeLast);
                    var dtLast = dtStart.AddMilliseconds(longTimeLast);
                    var adds = (int)(DateTime.Now - dtLast).TotalSeconds;
                    Log.i(adds);
                    if (adds > 120)
                    {
                        if (isPauseCheck)
                        {
                            PlayOffTimeInter();
                        }
                        //UIMgr.S.OpenTopPanel(UIID.OfflinePanel, null, adds);
                    }
                }
            }
            if (isPauseCheck)
            {
                //PlayerInfoMgr.data.SetLastPlayTime(CustomExtensions.GetTimeStamp());
            }
            else
            {
                StartTimeRecord();
            }*/
        }
        #endregion

        public bool CheckisOnline()
        {
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                return false;
            }

            return true;
        }

        void PlayOffTimeInter()
        {
            //if (AdsMgr.S.isNoAdsMode)
            //    return;
            //AdDisplayer.Builder()
            //    .SetPlacementID(Define.AD_PLACEMENT_INTER)
            //    .Show("OffTimeInter");
        }
    }
}
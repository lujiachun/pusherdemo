﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using UnityEngine.UI;
using System;

namespace GameWish.Game
{
    [Serializable]
    public class CameraSize 
    {
        public int key;
        public int filedView;
    }
    
    public class CameraRoot : TMonoSingleton<CameraRoot>
    {

        [SerializeField]
        private CameraSize[] m_RawRatio;
        [SerializeField]
        private Transform m_CameraRoot;
        public void Init() 
        {
            Camera.main.fieldOfView = 64;
        }

        private int GetIndex() 
        {
            float width = Screen.width;
            float length = Screen.height;

            int index =  Mathf.CeilToInt((length * 9) / width);
            return index;
        }

        private Transform GetRoot(int name) 
        {


           return transform.Find(name.ToString());

        }

    }
}
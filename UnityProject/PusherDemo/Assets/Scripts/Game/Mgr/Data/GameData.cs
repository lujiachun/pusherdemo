﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System;
using QuickEngine.Extensions;

namespace GameWish.Game
{

    public class RedeemRecordData : IDataClass 
    {
        public string ItemName;
        public string Time;
        public string State;

        public void SetState(string state) 
        {
            State = state;
            SetDataDirty();
        }
    }

    public class RedeemRewardData : IDataClass 
    {
        public int id;
        public long current;
        public bool isFinish;

        public long GetCurrent() 
        {
            return current;
        }

        public void SetCurrent(int count) 
        {
            current = count;
            SetDataDirty();
        }
    }

    public class GameData : IDataClass
    {
        public EInt m_100MultiDollar;
        public long m_CoinsCount;
        public EInt m_TokenCount;
        public EInt m_AmazonCount;

        public long m_LastRecord = -1;

        public int m_2Count;
        public int m_5Count;
        public int m_10Count;
        public int m_20Count;

        public List<RedeemRecordData> m_RedeemDataArray;
        public List<RedeemRewardData> m_RedeemRewardArray;
        public List<int> m_DecorationsArray;

        private Dictionary<int, RedeemRewardData> m_RedeemRewardDic = new Dictionary<int, RedeemRewardData>();


        public GameData()
        {
            SetDirtyRecorder(GameInfoMgr.dataDirtyRecorder);
        }

        public override void InitWithEmptyData()
        {
            m_100MultiDollar = 0;
            m_CoinsCount = 0;
            m_TokenCount = 40;
        }

        public override void OnDataLoadFinish()
        {
            EventSystem.S.Register(EngineEventID.OnApplicationFocusChange, OnApplicationFocusChange);

            if (CustomExtensions.CheckIsNewDay() > 0) 
            {
                ResetData();
            }

            if (m_RedeemDataArray == null) 
            {
                m_RedeemDataArray = new List<RedeemRecordData>();
            }
            if (m_RedeemRewardArray == null)
                m_RedeemRewardArray = new List<RedeemRewardData>();
            if (m_DecorationsArray == null)
                m_DecorationsArray = new List<int>();

            m_RedeemRewardArray.ForEach(v => 
            {
                m_RedeemRewardDic.Add(v.id , v);
                v.SetDirtyRecorder(GameInfoMgr.dataDirtyRecorder);
            });


            TDRedeemRewardTable.dataList.ForEach(v =>
            {

                if (!m_RedeemRewardDic.ContainsKey(v.id)) 
                {
                    var data = new RedeemRewardData();
                    data.SetDirtyRecorder(GameInfoMgr.dataDirtyRecorder);
                    data.id = v.id;
                    data.current = 0;
                    data.isFinish = false;
                    m_RedeemRewardArray.Add(data);
                    m_RedeemRewardDic.Add(v.id, data);
                    data.SetDataDirty();
                    SetDataDirty();
                }
            });
        }

        public void Add2Count() 
        {
            m_2Count++;
            SetDataDirty();
        }

        public void Add5Count()
        {
            m_5Count++;
            SetDataDirty();
        }

        public void Add10Count()
        {
            m_10Count++;
            SetDataDirty();
        }

        public void Add20Count()
        {
            m_20Count++;
            SetDataDirty();
        }

        public void AddAmazomCount(int count) 
        {
            m_AmazonCount += count;
            SetDataDirty();
            m_RedeemRewardDic[1].SetCurrent(m_AmazonCount);
            EventSystem.S.Send(EventID.OnAmazonCountUpdate);
        }

        public int GetAmazonCount() 
        {  
            return m_AmazonCount;
        }

        public bool isHasDecorations(int id) 
        {
            return m_DecorationsArray.Contains(id);
        }

        public void AddDecorations(int id) 
        {
            if (!m_DecorationsArray.Contains(id)) 
            {
                m_DecorationsArray.Add(id);
                SetDataDirty();
            }
        }

        private void ResetData() 
        {
            m_10Count = m_20Count = m_2Count = m_5Count = 0;
        }

        private void OnApplicationFocusChange(int key, params object[] param)
        {
            if (param == null || param.Length <= 0) 
            {
                return;
            }

            bool isfocus = (bool)param[0];

            if (isfocus) 
            {
                if (m_LastRecord > -1)
                {
                    long times = long.Parse(CustomExtensions.GetTimeStampUniSec());
                    int totalTimes = (int)(times - m_LastRecord);
                    m_TokenCount = Mathf.Clamp(m_TokenCount +(totalTimes/ Define.GameDefine.SUPPLY_TIME),0, 40);
                    SetDataDirty();
                    if (m_TokenCount < 40) 
                    {
                        
                    }
                }
            }
        }

        public float GetDollar() 
        {
            return (float)m_100MultiDollar / 100;
        }

        public void AddDollar(float dol) 
        {
            if (dol > 0) 
            {
                AudioMgr.S.PlaySoundShoot("moneyget");
            }

            m_100MultiDollar += (int)(dol * 100);
            SetDataDirty();
            m_RedeemRewardDic[2].SetCurrent(m_AmazonCount);
            EventSystem.S.Send(EventID.OnDolloarCountUpdate);
        }

        public long GetCoinsCount() 
        {
            return m_CoinsCount;
        }
        public void AddCoins(int val)
        {
            m_CoinsCount += val;
            SetDataDirty();
            m_RedeemRewardDic[3].SetCurrent(m_AmazonCount);
            EventSystem.S.Send(EventID.OnCoinsCountUpdate);
        }

        public int GetTokenCount()
        {
            return m_TokenCount;
        }

        public bool AddToken(int val)
        {
            if ((m_TokenCount + val) < 0) 
            {
                return false;
            }

            m_TokenCount = Mathf.Clamp(m_TokenCount + val, 0 ,40);
            SetDataDirty();
            EventSystem.S.Send(EventID.OnTokensCountUpdate);

            return true;
        }

        public void AddRedeemRecord() 
        {
            var rec = new RedeemRecordData();
            rec.SetDirtyRecorder(GameInfoMgr.dataDirtyRecorder);

            rec.ItemName = "test";
            rec.Time = DateTime.Now.ToDdMmYySlash();
            rec.State = "Paid";

            rec.SetDataDirty();

            m_RedeemDataArray.Add(rec);
            SetDataDirty();
        }

        public RedeemRewardData GetRedeemRewardData(int id) 
        {
            return m_RedeemRewardDic[id];
        }
        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;


namespace GameWish.Game
{

    public class GameInfoMgr : DataClassHandler<GameData>
    {
        public static EventSystem itemEventSystem = new EventSystem();
        public static DataDirtyRecorder dataDirtyRecorder = new DataDirtyRecorder();

        public GameInfoMgr()
        {
            Load();
            EnableAutoSave();
        }
    }
}
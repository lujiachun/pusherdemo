﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System;
using DG.Tweening;

namespace GameWish.Game
{

    public class SceneItemMgr : TMonoSingleton<SceneItemMgr>
    {
        [SerializeField]
        private Transform[] m_CudeStage;
        [SerializeField]
        private Transform[] m_AllObs;

        /// <summary>
        /// 左边墙壁
        /// </summary>
        [SerializeField]
        private Transform m_WallL;
        /// <summary>
        /// 右边墙壁
        /// </summary>
        [SerializeField]
        private Transform m_WallR;

        public bool m_CubeStageState = true;


        public void Init() 
        {
            EventSystem.S.Register(EventID.OnObstacleClearStateChange, OnObstacleClearStateChange);
            EventSystem.S.Register(EventID.OnSlotReward, OnSlotReward);

            EventSystem.S.Register(EventID.OnWallOpen, OnWallOpen);
            EventSystem.S.Register(EventID.OnWallClose, OnWallClose);
        }

        /// <summary>
        /// 墙打开
        /// </summary>
        /// <param name="key"></param>
        /// <param name="param"></param>
        private void OnWallOpen(int key, params object[] param)
        {
            m_WallL.DOLocalMoveY(7, 1f);
            m_WallR.DOLocalMoveY(7, 1f);
        }

        /// <summary>
        /// 墙关闭
        /// </summary>
        /// <param name="key"></param>
        /// <param name="param"></param>
        private void OnWallClose(int key, params object[] param)
        {
            m_WallL.DOLocalMoveY(-9, 1f);
            m_WallR.DOLocalMoveY(-9, 1f);
        }

        




        private void OnSlotReward(int key, params object[] param)
        {

            if (param.Length == 0 || param == null) return;

            var result = (SlotRewardType)param[0];


            if (result == SlotRewardType.Prize777)
            {
                SetAllobsState(false);
            }
        }

        private void OnObstacleClearStateChange(int key, params object[] param)
        {

            if (param.Length == 0 || param == null) return;

            bool result = (bool)param[0];
            m_CubeStageState = result;

            for (int i = 0; i < m_CudeStage.Length; i++)
            {
                m_CudeStage[i].gameObject.SetActive(result);
            }
        }

        public void SetAllobsState(bool state) 
        {
            for (int i = 0; i < m_AllObs.Length; i++)
            {
                m_AllObs[i].gameObject.SetActive(state);
            }

            if (m_CubeStageState) return;

            for (int i = 0; i < m_CudeStage.Length; i++)
            {
                m_CudeStage[i].gameObject.SetActive(m_CubeStageState);
            }
        }
    }
}
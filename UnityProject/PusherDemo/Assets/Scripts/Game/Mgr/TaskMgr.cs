﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{

    public class TaskMgr : TSingleton<TaskMgr>
    {

        private int currntId = -1;

        public void InitTask() 
        {
            PostNextTask();
        }

        public void PostNextTask() 
        {
            var data = TDTaskTable.GetNextTask(currntId);

            if (data != null) 
            {
                EventSystem.S.Send(EventID.OnTaskChange, data);
            }
        }
        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System;

namespace GameWish.Game
{

    public class BGMgr : TMonoSingleton<BGMgr>
    {
        [SerializeField]
        private GameObject m_BgNormal;

        public void InitBG() 
        {
            m_BgNormal.SetActive(true);

            EventSystem.S.Register(EventID.OnDoubleModeStateChange, OnDoubleModeStateChange);
        }

        private void OnDoubleModeStateChange(int key, params object[] param)
        {
            if (param == null || param.Length <= 0) 
            {
                return;
            }

            bool state = (bool)param[0];

            m_BgNormal.SetActive(!state);
        }
    }
}